package de.kawaida.kjgame.animation;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Event;

import de.venjinx.core.libgdx.entity.Character;

public class Animation {

    public class AnimationEvent extends Event {

        public Animation animation;
        public String name = "";

        public AnimationEvent(Animation anim, String eventName) {
            animation = anim;
            name = anim.getName() + "_" + eventName;
        }

        //        public AnimationEvent(Animation src, VJXEntity target, String name) {
        //            setListenerActor(src);
        //            setTarget(target);
        //            setBubbles(false);
        //            setStage(src.getStage());
        //        }

        public String getName() {
            return name;
        }

        public Animation getAnimation() {
            return animation;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private Character who;

    private String actorName;
    private String what;
    private Vector2 offset;
    private Vector2 size;

    private float time;
    private float delay;
    private int frameCount = 0;
    private int timesPlayed = 0;
    private int repeat = 1;

    private int currentFrame = -1;
    private int nextFrame = 0;

    private int startFrame = 0, endFrame = 0;

    private boolean loop = false;
    private boolean loopBack = false;
    private boolean reverse = false;
    private boolean xFlipped = false;
    private boolean yFlipped = false;

    private AtlasRegion[] frames;

    public Animation(Character who, String what) {
        this(new AtlasRegion[0], who, what);
    }

    public Animation(AtlasRegion[] frames, Character who, String what) {
        offset = new Vector2();
        size = new Vector2();

        this.who = who;
        actorName = who.getName();
        this.what = what;
        delay = 1 / 5f;

        setFrames(frames);
    }

    public void flip(boolean flipX, boolean flipY) {
        xFlipped = flipX;
        yFlipped = flipY;

        for (AtlasRegion ar : frames)
            ar.flip(flipX, flipY);
    }

    public void setDelay(float delay) {
        this.delay = delay;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean looping() {
        return loop;
    }

    public boolean loopingBack() {
        return loopBack;
    }

    public void setLoopBack(boolean loopBack) {
        this.loopBack = loopBack;
    }

    public void setFrames(AtlasRegion[] frames) {
        setFrames(frames, 0, frames.length);
    }

    public void setFrames(AtlasRegion[] frames, int start, int end) {
        this.frames = frames;

        if (frames == null || frames.length == 0) return;

        startFrame = start;
        endFrame = end;
        frameCount = end - start;

        AtlasRegion frame = null;
        for (int i = startFrame; i < endFrame; i++) {
            frame = frames[i];
            frame.offsetY = frame.originalHeight - frame.offsetY
                    - frame.getRegionHeight();

            offset.x += frame.offsetX;
            offset.y += frame.offsetY;

            size.x += frame.getRegionWidth();
            size.y += frame.getRegionHeight();
        }

        if (frame != null) {
            size.scl(1f / frameCount);

            offset.scl(1f / frameCount);
        }

        reset();
    }

    public void update(float deltaT) {
        if (delay <= 0 || frameCount < 2) return;
        time += deltaT;
        if (time >= delay) {
            update();
            time -= delay;
        }
    }

    public boolean isFinished() {
        return !loop && !(timesPlayed < repeat);
    }

    private void update() {
        if (isFinished()) return;

        // remove offset
        int normStart = currentFrame - startFrame;

        // get next frame based on running direction and total amount of frames
        nextFrame = (normStart + (reverse ? -1 : 1)) % frameCount;

        // reset offset
        nextFrame += startFrame;

        // Fix negative position when running backwards
        if (nextFrame < startFrame) nextFrame = endFrame - 1;

        if (!loopBack) {
            if (nextFrame == (reverse ? endFrame - 1 : startFrame)) {
                timesPlayed++;
                AnimationEvent e = new AnimationEvent(this, "restart");
                who.fire(e);

                if (isFinished()) {
                    e = new AnimationEvent(this, "finished");
                    who.fire(e);
                    return;
                }
            }
        } else {
            if (nextFrame == (reverse ? endFrame - 1 : startFrame)) {
                if (nextFrame == endFrame - 1 && reverse) {
                    timesPlayed++;
                    AnimationEvent e = new AnimationEvent(this, "restart");
                    who.fire(e);
                }

                reverse = !reverse;
                nextFrame = currentFrame + (reverse ? -1 : 1);
                return;
            }
        }
        currentFrame = nextFrame;
    }

    public void reset() {
        time = 0;
        currentFrame = startFrame;
        timesPlayed = 0;
        reverse = false;
    }

    //    public void setFaceDirection(KJDirection direction) {
    //        reset();
    //        loopStart = direction == KJDirection.Right ? loopStart : frameCount / 2;
    //        loopEnd = direction == KJDirection.Right ? frameCount / 2 : loopEnd;
    //        currentFrame = loopStart;
    //        timesPlayed = 0;
    //    }

    public String getName() {
        return actorName + "_" + what;
    }

    public void setRepeat(int count) {
        repeat = count;
    }

    public AtlasRegion[] getFrames() {
        return frames;
    }

    public AtlasRegion getFrame() {
        return frames[currentFrame];
    }

    public AtlasRegion getFrame(float deltaT) {

        return frames[currentFrame];
    }

    public void setStartFrame(int frameNr) {
        startFrame = frameNr;
        frameCount = endFrame - startFrame;
    }

    public int getStartFrame() {
        return startFrame;
    }

    public void setEndFrame(int frameNr) {
        endFrame = frameNr;
        frameCount = endFrame - startFrame;
    }

    public int getEndFrame() {
        return endFrame;
    }

    public int getTimesPlayed() {
        return timesPlayed;
    }

    public int getFrameCount() {
        return endFrame - startFrame;
    }

    public Vector2 getSize() {
        return size;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public boolean flipped() {
        return xFlipped || yFlipped;
    }

    public boolean xFlipped() {
        return xFlipped;
    }

    public boolean yFlipped() {
        return yFlipped;
    }

    @Override
    public String toString() {
        String s = what + "(" + getFrameCount() + "):";
        s += "\n  size        : " + size;
        s += "\n  offset      : " + offset;
        s += "\n  currentFrame: " + currentFrame;
        s += "\n  nextFrame   : " + nextFrame;
        s += "\n  start       : " + startFrame;
        s += "\n  end         : " + endFrame;
        s += "\n  loop        : " + loop;
        s += "\n  loopBack    : " + loopBack;

        return s;
    }
}