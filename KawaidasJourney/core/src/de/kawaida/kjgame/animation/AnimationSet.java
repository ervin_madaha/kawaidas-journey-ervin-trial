package de.kawaida.kjgame.animation;

import java.util.Collection;
import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.venjinx.core.libgdx.entity.Character;

public class AnimationSet {

    private Character who;

    private String actorName;
    private Vector2 size;
    private boolean xFlipped = false;
    private boolean yFlipped = false;

    private HashMap<String, Animation> animations;
    private Animation currentAnimation;
    private String returnAnimation;

    public AnimationSet(Character c) {
        who = c;
        actorName = c.getName();

        animations = new HashMap<String, Animation>();
        size = new Vector2();
    }

    public boolean update(float deltaT) {
        currentAnimation.update(deltaT);

        if (currentAnimation.isFinished()) {
            //            if (actorName.contains("kawaida"))
            setAnimation(returnAnimation, "");
            return true;
        }

        return false;
    }

    public void flip(boolean flipX, boolean flipY) {
        xFlipped = flipX;
        yFlipped = flipY;

        Collection<Animation> s = animations.values();
        for (Animation a : s)
            a.flip(flipX, flipY);
    }

    public void load(TextureAtlas textureAtlas) {
        Array<AtlasRegion> a = textureAtlas.getRegions();
        if (a.size == 0) return;

        AtlasRegion frame;
        String frameName, what;
        //        int frameID, frameDirection;
        Animation anim = null;
        AtlasRegion[] frames = new AtlasRegion[a.size];
        size.set(a.first().originalWidth, a.first().originalHeight);

        int count = 0, frameNr = 0;
        for (int i = 0; i < a.size; i++) {
            count = 0;
            frame = a.get(i);
            frameName = frame.name.split("#")[0];
            //            frameID = Integer.parseInt(frameData[1]);
            frames[i] = frame;

            String[] nameSplit = frameName.split("_");
            what = nameSplit[nameSplit.length - 1];

            anim = new Animation(who, what);

            count++;
            frameNr++;
            while (i + count < a.size) {
                frame = a.get(i + count);
                frameName = frame.name.split("#")[0];
                frames[i + count] = frame;

                if (anim.getName().equals(frameName)) {
                    count++;
                    frameNr++;
                } else {
                    i--;
                    break;
                }
            }
            anim.setFrames(frames, frameNr - count, frameNr);
            animations.put(anim.getName(), anim);

            i += count;
        }
        currentAnimation = anim;
    }

    public Animation setAnimation(String animName, String next) {
        if (!animName.equals(currentAnimation.getName())
                        && animations.containsKey(animName)) {

            if (next != null && !next.equals("")) returnAnimation = next;

            if (animName.contains("die")) {
                returnAnimation = animName;
            }

            currentAnimation = animations.get(animName);

            //            currentAnimation.setLoop(loop);

            //            if (!loop) {
            //                returnAnimation = next;
            //            } else {
            //                returnAnimation = currentAnimation.getName();
            //            }
        }
        return currentAnimation;
    }

    public void setNextAnimation(String nextAnim) {
        returnAnimation = nextAnim;
    }

    //    public Animation setAnimation(String animName) {
    //        if (getName().contains("palm"))
    //            System.out.println("set anim " + animName);
    //        if (!animName.equals(currentAnimation.getName())
    //                && animations.containsKey(animName)) {
    //            returnAnimation = "";
    //            if (!animName.contains("die"))
    //                returnAnimation = currentAnimation.getName();
    //
    //            currentAnimation = animations.get(animName);
    //        }
    //
    //        return currentAnimation;
    //    }

    public AtlasRegion getFrame() {
        return currentAnimation.getFrame();
    }

    public Animation getAnimation(String name) {
        return animations.get(name);
    }

    public Animation getCurrent() {
        return currentAnimation;
    }

    public HashMap<String, Animation> getAnimations() {
        return animations;
    }

    public void addAnimation(String animName, Animation animation) {
        animations.put(animName, animation);
    }

    public int getAnimCount() {
        return animations.size();
    }

    public Vector2 getSize() {
        return size;
    }

    public String who() {
        return actorName;
    }

    public String getName() {
        return "anims_" + actorName;
    }

    public boolean flipped() {
        return xFlipped || yFlipped;
    }

    public boolean xFlipped() {
        return xFlipped;
    }

    public boolean yFlipped() {
        return yFlipped;
    }

    public void dispose() {
        currentAnimation = null;
    }
}