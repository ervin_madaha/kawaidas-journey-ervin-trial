package de.kawaida.kjgame.controllers;

import java.util.HashMap;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import de.venjinx.core.libgdx.VJXAssetManager;

public class AudioController {

	private VJXAssetManager am;
	
    private HashMap<String, Sound> sounds = new HashMap<>();
    private HashMap<String, Music> music = new HashMap<>();

    public AudioController(VJXAssetManager assetMngr) {
        sounds = new HashMap<>();
        am = assetMngr;
    }

//    public long playSound(String name, boolean loop) {
//        return playSound(name, loop, 1, 1, 0);
//    }
//
//    public long playSound(String name, boolean loop,
//                    float volume, float pitch, float pan) {
//        Sound s = sounds.get(name);
//        
//        boolean playing = soundIDs.containsKey(name);
//
//        if (playing && loop) return soundIDs.get(name);
//
//        if (playing && !loop) s.stop();
//
//        long id;
//        if (loop)
//            id = s.loop(volume, pitch, pan);
//        else id = s.play(volume, pitch, pan);
//
//        soundIDs.put(name, id);
//        return id;
//    }

//    public void stopSound(String name) {
//        if (!soundIDs.containsKey(name)) return;
//
//        sounds.get(name).stop(soundIDs.remove(name));
//    }

//    public void stopAllSounds() {
//        for (String s : sounds.keySet())
//            stopSound(s);
//    }

    //    public Sound loadSound(String name, String file) {
    //        if (!sounds.containsKey(name))
    //            sounds.put(name, Gdx.audio.newSound(Gdx.files.internal(file)));
    //        return sounds.get(name);
    //    }
    //
    //    //    public Sound getSound(String name) {
    //    //        return sounds.get(name);
    //    //    }
    //
    //    public Music loadMusic(String name, String file) {
    //        if (!music.containsKey(name))
    //            music.put(name, Gdx.audio.newMusic(Gdx.files.internal(file)));
    //
    //        return music.get(name);
    //    }
    //
    //    public Music getMusic(String name) {
    //        return music.get(name);
    //    }
    //
    //    public void disposeSound(String name) {
    //        sounds.remove(name).dispose();
    //    }
    //
    //    public void dispose() {
    //        for (Sound s : sounds.values())
    //            s.dispose();
    //        sounds.clear();
    //    }
}
