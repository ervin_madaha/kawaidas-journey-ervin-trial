package de.kawaida.kjgame.controllers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.entity.VJXEntity;

public class CollisionControl implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture f;
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        short cat0 = fa.getFilterData().categoryBits;
        short cat1 = fb.getFilterData().categoryBits;

        VJXData fData0 = (VJXData) fa.getUserData();
        VJXData fData1 = (VJXData) fb.getUserData();

        VJXEntity e0 = fData0.getEntity();
        VJXEntity e1 = fData1.getEntity();

        if (e1 instanceof Avatar) {
            f = fa;
            fa = fb;
            fb = f;

            fData0 = (VJXData) fa.getUserData();
            fData1 = (VJXData) fb.getUserData();

            e0 = fData0.getEntity();
            e1 = fData1.getEntity();
        }

        if (!e0.isAlive() || !e1.isAlive()) return;

        int s = cat0 | cat1;
        if (isCategory((short) s, VJXPhys.NO_PASS)) {

            //            if (!fData0.getName().equals("feet")) return;

            if (!e0.isOnGround()) {
                if (e0.getBody().getLinearVelocity().x == 0) {
                    e0.setStatus(VJXStatus.IDLE);
                    if (e0 instanceof Avatar)
                    e0.stopSound("fx_kawaida_walk");
                } else {
                    if (e0.getStatus() != VJXStatus.RUN) {
                        if (e0 instanceof Avatar)
                        e0.playSound("fx_kawaida_walk", true);
                    }
                    e0.setStatus(VJXStatus.RUN);
                }
                if (e0 instanceof Avatar)
                    e0.playSound("fx_kawaida_land_ground", false);
            }
            e0.incrOnGround();
            return;
        }

        int s1 = VJXPhys.PLAYER.id | VJXPhys.TRIGGER.id;
        if ((s & s1) == s1) {
            e0.trigger(fb);
            return;
        }

        s1 = VJXPhys.ENEMY.id | VJXPhys.WEAPON.id;
        if ((s & s1) == s1) {
            if (isCategory(cat0, VJXPhys.ENEMY)) {
                e1.playSound("fx_item_cocos_hit", false);
                e0.stopAllSounds();
                e0.setStatus(VJXStatus.DIE, true);

                e1.getBody().setLinearVelocity(0f, 2f);
            } else {
                e0.playSound("fx_item_cocos_hit", false);
                e1.stopAllSounds();
                e1.setStatus(VJXStatus.DIE, true);

                e0.getBody().setLinearVelocity(0f, 2f);
            }
            return;
        }

        s1 = VJXPhys.PLAYER.id | VJXPhys.ITEM.id;
        if ((s & s1) == s1) {
            e1.playSound("fx_" + e1.getName() + "_collected", false);
        }

        e0.collideWith(e1);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        VJXData fData0 = (VJXData) fa.getUserData();
        VJXData fData1 = (VJXData) fb.getUserData();

        VJXEntity e0 = fData0.getEntity();
        VJXEntity e1 = fData1.getEntity();

        short cat0 = e0.getCategory();
        short cat1 = e1.getCategory();

        int s = cat0 | cat1;
        if (isCategory((short) s, VJXPhys.NO_PASS)) {
            if (isCategory(cat0, VJXPhys.NO_PASS)) e0 = e1;

            e0.decrOnGround();
            return;
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private boolean isCategory(short cat, VJXPhys physCat) {
        return (cat & physCat.id) > 0;
    }
}