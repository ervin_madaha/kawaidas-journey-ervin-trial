package de.kawaida.kjgame.entities.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.items.Banana;
import de.kawaida.kjgame.entities.items.PalmTree;
import de.kawaida.kjgame.entities.items.Totem;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXTypes.VJXDirection;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.entity.Character;
import de.venjinx.core.libgdx.entity.Item;
import de.venjinx.core.libgdx.entity.VJXEntity;
import de.venjinx.core.libgdx.entity.Weapon;

public class Avatar extends Character {

    private VJXGame game;

    private float jumpHeight = 5f;

    private Weapon weapon;
    private Totem totem;
    private Banana banana;

    private boolean attacking = false;

    public Avatar(VJXGame game) {
        super(BodyType.DynamicBody);
        b2dCategory |= VJXPhys.PLAYER.id;
        b2dMask |= (short) (VJXPhys.ITEM.id | VJXPhys.ENEMY.id);

        setName("kawaida");

        this.game = game;

        banana = new Banana("item_banana");
        banana.set(1);
        banana.setMax(4);

        totem = new Totem("item_totem");
        totem.setMax(1);

        moveSpeed = 5f;
    }

    public void jump() {
        setStatus(VJXStatus.JUMP);
        stopSound("fx_kawaida_walk");
        playSound("fx_kawaida_jump", false);
    }

    public void walk() {
        int dir = faceDirection == VJXDirection.Left ? -1 : 1;
        Vector2 bodyVel = body.getLinearVelocity();
        bodyVel.x = game.getPlayer().getAvatar().getSpeed() * dir;
        body.setLinearVelocity(bodyVel);

        if (isOnGround() && status != VJXStatus.WALK) {
            setStatus(VJXStatus.WALK);
            playSound("fx_kawaida_walk", true);
        }
    }

    public void run() {
        int dir = faceDirection == VJXDirection.Left ? -1 : 1;
        Vector2 bodyVel = body.getLinearVelocity();
        bodyVel.x = game.getPlayer().getAvatar().getSpeed() * dir;
        body.setLinearVelocity(bodyVel);

        if (isOnGround() && status != VJXStatus.RUN) {
            setStatus(VJXStatus.RUN);
            playSound("fx_kawaida_walk", true);
        }
    }

    public void stop() {
        if (isOnGround())
        setStatus(VJXStatus.IDLE);

        Vector2 bodyVel = body.getLinearVelocity();

        body.setLinearVelocity(0, bodyVel.y);
        stopSound("fx_kawaida_walk");
    }

    @Override
    public void userAct(float deltaT) {
        if (banana.getAmount() == 0 && status != VJXStatus.HIT
                        && status != VJXStatus.DIE) {
            setStatus(VJXStatus.DIE, true);
            stopAllSounds();
            playSound("fx_kawaida_die", false);
        }
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
    }

    @Override
    public void collideWith(VJXEntity other) {

        // player hits collectable
        if (other instanceof Item) {
            collect((Item) other);
            game.getScreen().getLevel().markRemove(other);
            game.getScreen().getHUD().updateHUD();
            return;
        }

        // player hits enemy
        if (other instanceof Enemy) {
            if (!status.blocking)
                if (!attacking) {
                    banana.decr();
                    game.getScreen().getHUD().updateHUD();
                    setStatus(VJXStatus.HIT);
                    playSound("fx_kawaida_hit", false);
                    getBody().setLinearVelocity(0, 0);
                } else {
                    other.setStatus(VJXStatus.DIE);

                    Vector2 v = body.getLinearVelocity();
                    body.setLinearVelocity(v.x, 6f);
                    setStatus(VJXStatus.JUMP, true);
                    playSound("fx_kawaida_jump", false);
                    attacking = false;
                }
            return;
        }
        if (other instanceof PalmTree) {
            other.setStatus(VJXStatus.TRIGGERED);
            ((PalmTree) other).getAnimations().setNextAnimation("palm_grown");
            return;
        }
    }

    @Override
    public void trigger(Fixture triggerFixture) {
        VJXData fData = (VJXData) triggerFixture.getUserData();
        String name = fData.getName();
        if (name.contains("head")) {
            if (name.contains("trap")) {
                fData.getEntity().setStatus(VJXStatus.TRIGGERED);
                collideWith(fData.getEntity());
            } else attacking = true;
        }
    }

    public void collect(Item c) {
        if (c.getName().equals("item_banana")) {
            banana.incr();
            return;
        }
        if (c.getName().equals("item_sun")) {
            game.getPlayer().getSun().incr();
            return;
        }
        if (c.getName().equals("item_totem")) {
            totem.incr();
            return;
        }
        if (c.getName().equals("item_cocos")) {
            weapon.incr();
            return;
        }
    }

    public void throwCocos() {
        if (weapon.isEmpty()) return;
        setStatus(VJXStatus.THROW);

        if (faceDirection == VJXDirection.Right) {
            weapon.setPosition(getX() + getWidth(), getY() + getHeight());
            weapon.getBody().setLinearVelocity(10, 1);
        } else {
            weapon.setPosition(getX(), getY() + getHeight());
            weapon.getBody().setLinearVelocity(-10, 1);
        }

        weapon.decr();
        playSound("fx_kawaida_throw", false);
    }

    public Banana getBanana() {
        return banana;
    }

    public Totem getTotem() {
        return totem;
    }

    public void setWeapon(Weapon w) {
        weapon = w;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public float getJumpHeight() {
        return jumpHeight;
    }

    @Override
    public void visit(Acceptor acceptor) {
        // player visit collectable
        if (acceptor instanceof Item) {
            collect((Item) acceptor);
            game.getScreen().getLevel().markRemove((Item) acceptor);
            game.getScreen().getHUD().updateHUD();
            return;
        }
    }

    @Override
    public void hit(Acceptor acceptor) {
        // player hits enemy
        if (acceptor instanceof Enemy) {
            ((Enemy) acceptor).setStatus(VJXStatus.DIE);

            Vector2 v = body.getLinearVelocity();
            body.setLinearVelocity(v.x, 6f);
            setStatus(VJXStatus.JUMP, true);
            playSound("fx_kawaida_jump", false);
            attacking = false;

            if (!status.blocking) if (!attacking) {
            } else {
            }
            return;
        }

    }

    @Override
    public void trigger(Acceptor vjxE) {
    }

    @Override
    public void spawnActor(World world) {

    }
}