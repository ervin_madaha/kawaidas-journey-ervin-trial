package de.kawaida.kjgame.entities.actors;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.entity.Character;
import de.venjinx.core.libgdx.entity.VJXEntity;

public abstract class Enemy extends Character {

    public Enemy() {
        super(BodyType.DynamicBody);
        b2dCategory |= VJXPhys.ENEMY.id;
        b2dMask |= VJXPhys.PLAYER.id | VJXPhys.WEAPON.id;
    }

    @Override
    public void visit(Acceptor vjxE) {

    }

    @Override
    public void hit(Acceptor acceptor) {
        if (acceptor instanceof Avatar) {
            Avatar p = (Avatar) acceptor;
            p.getBanana().decr();
            p.setStatus(VJXStatus.HIT);
            p.getBody().setLinearVelocity(0, 0);
            p.playSound("fx_kawaida_hit", false);

        }
        //        game.getScreen().getHUD().updateHUD();
    }

    @Override
    public void trigger(Acceptor vjxE) {

    }

    @Override
    public void collideWith(VJXEntity other) {
        if (other instanceof Avatar)
            other.collideWith(this);
    }

    @Override
    public void trigger(Fixture triggerFixture) {
    }
}