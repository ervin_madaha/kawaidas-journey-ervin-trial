package de.kawaida.kjgame.entities.actors;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.entity.VJXEntity;

public class Ground extends VJXEntity {

    private float groundLvl = 0;
    private float lvlWidth = 0;
    private ChainShape rootShape;
    //    private Vector2 prevVert, nextVert;
    private LinkedList<Vector2> chainVerts;

    public Ground() {
        this(0, 0);
    }

    public Ground(float width, float groundLvl) {
        super(BodyType.StaticBody);
        this.groundLvl = groundLvl;
        lvlWidth = width;
        //        prevVert = new Vector2();
        //        nextVert = new Vector2();
        chainVerts = new LinkedList<>();
        b2dCategory = VJXPhys.NO_PASS.id;
        b2dMask = VJXPhys.CHARACTER.id;
        setName("ground");
    }

    //    public void setLvlWidth(float width) {
    //        lvlWidth = width;
    //    }

    public void setGroundLvl(float height) {
        groundLvl = height;
    }

    public void setGroundWidth(float width) {
        lvlWidth = width;
    }

    public void addVertex(Vector2 vertex) {
        chainVerts.add(vertex);
    }

    public void addPlatform(Vector2[] vertices) {
        ChainShape cs = new ChainShape();

        if (vertices != null && vertices.length > 0) {
            cs.createChain(vertices);

            FixtureDef fixDef = new FixtureDef();
            fixDef.shape = cs;
            fixDef.filter.categoryBits = b2dCategory;
            fixDef.filter.maskBits = b2dMask;
            fixDef.restitution = 0f;
            fixDef.friction = 0f;

            boundFixture = body.createFixture(fixDef);
            boundFixture.setUserData(new VJXData("bound", this));
        }
    }

    @Override
    public void collideWith(VJXEntity other) {
    }

    @Override
    public void trigger(Fixture triggerFixture) {

    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    protected void createFixtures() {
        body.destroyFixture(boundFixture);
        rootShape = new ChainShape();

        if (chainVerts.size() == 0 && lvlWidth != 0) {
            chainVerts.add(new Vector2(0, groundLvl + 2000).scl(1 / 100f));
            chainVerts.add(new Vector2(0, groundLvl).scl(1 / 100f));
            chainVerts.add(new Vector2(lvlWidth, groundLvl).scl(1 / 100f));
            chainVerts.add(new Vector2(lvlWidth, groundLvl + 2000)
                            .scl(1 / 100f));
        }

        if (chainVerts.size() > 0) {
            rootShape.createChain(chainVerts.toArray(new Vector2[chainVerts.size()]));

            FixtureDef fixDef = new FixtureDef();
            fixDef.shape = rootShape;
            fixDef.filter.categoryBits = b2dCategory;
            fixDef.filter.maskBits = b2dMask;
            fixDef.restitution = 0f;
            fixDef.friction = 0f;

            boundFixture = body.createFixture(fixDef);
            boundFixture.setUserData(new VJXData("bound", this));

            rootShape = (ChainShape) boundFixture.getShape();
        }
        boundShape = rootShape;
    }

    public int getChainSize() {
        return chainVerts.size();
    }

    @Override
    public void visit(Acceptor vjxE) {
    }

    @Override
    public void hit(Acceptor vjxE) {

    }

    @Override
    public void trigger(Acceptor vjxE) {

    }

    @Override
    public void spawnActor(World world) {

    }

}