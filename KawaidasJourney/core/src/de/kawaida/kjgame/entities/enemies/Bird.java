package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.kjgame.entities.actors.Enemy;
import de.venjinx.core.libgdx.VJXTypes.VJXDirection;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;

public class Bird extends Enemy {

    private float posT = 0;
    private float waveLength = 50;
    private float amplitude = 1.5f;

    public long soundID;

    public Bird() {
        super();
        setName("jz_bird1");

        moveSpeed = 1.5f;
    }

    public void flap() {
        getBody().setLinearVelocity(-moveSpeed, 1.8f);

        playSound("fx_jz_bird1_fly", false);
    }

    @Override
    public void userAct(float deltaT) {
        Avatar p = getStage().getPlayer().getAvatar();
        if (getX() + getWidth() < -100 && spawned) {
            setStatus(VJXStatus.IDLE, true);

            float f = getY();
            setPosition(p.getX() + 1000f, f + 0);
            getBody().setLinearVelocity(0f, 0f);
            stopSound("fx_jz_bird1_fly");
            return;
        }

        if (p.getCenter().dst(getCenter()) < 400) {
            if (status != VJXStatus.FLY && status != VJXStatus.DIE) {
                //            setFaceDirection(p.getCenter().x < getCenter().x ? VJXDirection.Left
                //                            : VJXDirection.Right);
                setStatus(VJXStatus.FLY);
                getBody().setGravityScale(.2f);
                //                getBody().setLinearVelocity(moveSpeed * deltaT, 1.8f);
                flap();

                for (Fixture f : body.getFixtureList()) {
                    b2dMask &= ~VJXPhys.NO_PASS.id;

                    Filter filter = new Filter();
                    filter.categoryBits = b2dCategory;
                    filter.maskBits = b2dMask;
                    f.setFilterData(filter);
                }
            }
        }

        //        posT += deltaT;
        if (status == VJXStatus.FLY) {
            if (p.getCenter().dst(getCenter()) > 700 || status == VJXStatus.DIE)
                stopSound("fx_jz_bird1_fly");

            float deltaX = moveSpeed * waveLength;
            float deltaY = (float) Math.sin(posT) * amplitude;

            deltaX = faceDirection != VJXDirection.Left ? -deltaX : deltaX;
            setPosition(getX() + deltaX * deltaT, getY() + deltaY);
        }

        //        if (p.getCenter().dst(getCenter()) >= 500 && status == VJXStatus.FLY) {
        //            setFaceDirection(p.getCenter().x < getCenter().x ? VJXDirection.Left
        //                            : VJXDirection.Right);
        //            setStatus(VJXStatus.IDLE);
        //        }
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}