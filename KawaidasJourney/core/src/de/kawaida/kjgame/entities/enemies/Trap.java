package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;

public class Trap extends Enemy {

    public Trap() {
        setName("trap");
        type = BodyType.KinematicBody;

        b2dCategory |= VJXPhys.TRIGGER.id;

        boundDef.isSensor = true;
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}