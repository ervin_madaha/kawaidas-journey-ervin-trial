package de.kawaida.kjgame.entities.enemies;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.kjgame.entities.actors.Enemy;
import de.venjinx.core.libgdx.VJXTypes.VJXDirection;

public class Turtle extends Enemy {

    public Turtle() {
        super();
        setName("turtle");
    }

    @Override
    public void userAct(float deltaT) {
        if (!isAlive()) return;

        Avatar p = getStage().getPlayer().getAvatar();
        if (p.getX() < getX()) {
            setDirection(VJXDirection.Right);
            body.setLinearVelocity(-moveSpeed,
                            body.getLinearVelocity().y + .12f);
        } else {
            setDirection(VJXDirection.Left);
            body.setLinearVelocity(moveSpeed,
                            body.getLinearVelocity().y + .12f);
        }
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void spawnActor(World world) {

    }
}