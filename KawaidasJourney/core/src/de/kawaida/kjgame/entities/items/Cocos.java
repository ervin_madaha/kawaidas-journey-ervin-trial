package de.kawaida.kjgame.entities.items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;

import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.entity.Weapon;

public class Cocos extends Weapon {

    public Cocos(String name) {
        super(name, BodyType.KinematicBody);

        if (name.contains("weapon")) type = BodyType.DynamicBody;
    }

    RotateByAction ra = new RotateByAction();

    @Override
    public void userAct(float deltaT) {
        //        ra.setAmount(720);
        //        ra.setDuration(2);
        //            ra.setActor(this);
        //            ra.act(deltaT);
        //        if (ra.getTime() >= ra.getDuration()) ra.reset();

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
    }

    @Override
    public void visit(Acceptor vjxE) {

    }

    @Override
    public void hit(Acceptor vjxE) {

    }

    @Override
    public void trigger(Acceptor vjxE) {

    }

    @Override
    public void spawnActor(World world) {

    }
}