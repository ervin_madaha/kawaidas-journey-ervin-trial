package de.kawaida.kjgame.entities.items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.entity.Valuable;

public class Totem extends Valuable {

    public Totem(String name) {
        super(name);
    }

    @Override
    public void userAct(float deltaT) {

    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {

    }

    @Override
    public void visit(Acceptor vjxE) {

    }

    @Override
    public void hit(Acceptor vjxE) {

    }

    @Override
    public void trigger(Acceptor vjxE) {

    }

    @Override
    public void spawnActor(World world) {

    }

}
