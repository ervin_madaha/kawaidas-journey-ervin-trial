package de.kawaida.kjgame.entities.level;

import java.util.ArrayDeque;
import java.util.ArrayList;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapImageLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.entities.actors.Ground;
import de.kawaida.kjgame.entities.enemies.Trap;
import de.kawaida.kjgame.entities.items.PalmTree;
import de.kawaida.kjgame.entities.neutral.Saidi;
import de.venjinx.core.libgdx.ParallaxCamera;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.entity.Consumable;
import de.venjinx.core.libgdx.entity.Obstacle;
import de.venjinx.core.libgdx.entity.VJXEntity;
import de.venjinx.core.libgdx.entity.VJXGroup;
import de.venjinx.core.libgdx.entity.Valuable;
import de.venjinx.core.libgdx.entity.Weapon;

public class KJLevel extends VJXStage {
    private OrthographicCamera cam;

    private World b2dWorld;

    private String environment;
    private TiledMap map;
    private int lvlWidth;
    private int lvlHeight;
    private String bgMusic;

    // background textures
    private Texture sky;
    private Texture horizon;
    private Texture path;
    private Texture foreground;

    // background layers
    private TiledMapTileLayer skyBG;
    private TiledMapTileLayer horizonBG;
    private TiledMapTileLayer pathBG;

    // tiled map layers
    private MapLayer collectableLayer;
    private MapLayer enemyLayer;
    private TiledMapTileLayer terrain;
    private ArrayList<TiledMapTileLayer> bgLayers;
    private ArrayList<TiledMapTileLayer> fgLayers;

    private OrthogonalTiledMapRenderer tmr;

    private ArrayDeque<Long> removeIPs;

    // in-game scene graph nodes
    private VJXGroup gameWorldNode;

    private VJXGroup groundNode;

    private VJXGroup obstaclesNode;

    private VJXGroup charactersNode;
    private VJXGroup enemiesNode;

    private VJXGroup itemsNode;
    private VJXGroup valuablesNode;
    private VJXGroup consumableNode;

    private VJXGroup usablesNode;
    private VJXGroup weaponsNode;
    private VJXGroup toolsNode;

    private Ground ground;

    private int sunCount = 0;

    private Music music;

    public KJLevel(VJXGame game) {
        super(game);
        setName("default-empty-map");
        getRoot().setName("node_scene_root");

        removeIPs = new ArrayDeque<Long>();

        paraCam = new ParallaxCamera(game.targetRes.x, game.targetRes.y);
        cam = game.getCam();

        gameWorldNode = new VJXGroup("node_gameWorld");

        // scene graph nodes for level ground and obstacles
        groundNode = new VJXGroup("node_ground");
        obstaclesNode = new VJXGroup("node_obstacles");

        // scene graph node for characters
        charactersNode = new VJXGroup("node_characters");

        enemiesNode = new VJXGroup("node_enemies");
        charactersNode.addActor(enemiesNode);

        // scene graph node for items
        itemsNode = new VJXGroup("node_items");

        valuablesNode = new VJXGroup("node_valuables");
        consumableNode = new VJXGroup("node_consumables");

        itemsNode.addActor(valuablesNode);
        itemsNode.addActor(consumableNode);

        // scene graph node for usable items
        usablesNode = new VJXGroup("node_usables");

        toolsNode = new VJXGroup("node_tools");
        weaponsNode = new VJXGroup("node_weapons");

        usablesNode.addActor(weaponsNode);
        usablesNode.addActor(toolsNode);
        itemsNode.addActor(usablesNode);

        // add level root nodes to gameWorld node
        gameWorldNode.addActor(obstaclesNode);
        gameWorldNode.addActor(groundNode);
        gameWorldNode.addActor(charactersNode);
        gameWorldNode.addActor(itemsNode);

        bgLayers = new ArrayList<>();
        fgLayers = new ArrayList<>();

        tmr = new OrthogonalTiledMapRenderer(new TiledMap(), getBatch());
        bgMusic = "none";
    }

    public void setPath(String path, boolean internal) {

        path.replace("\\", "/");

        String[] sa = path.split("/");
        String lvlName = sa[sa.length - 1].split("\\.")[0];
        setName(lvlName);
        environment = lvlName.split("#")[0];

        map = game.assetMngr.loadMap(path, lvlName, internal);

        setupAssetDefs();
    }

    private void setupAssetDefs() {
        resources.clear();

        String dir = "sprites/objects/collectables/";

        // objects
        resources.add(new AssetDef<>("collectables",
                        dir + "collectables.atlas",
                        TextureAtlas.class));

        // item animations
        dir = "sprites/objects/";
        resources.add(new AssetDef<>("anims_item_sun",
                        dir + "anims_item_sun.atlas", TextureAtlas.class));
        resources.add(new AssetDef<>("anims_item_banana",
                        dir + "anims_item_banana.atlas", TextureAtlas.class));
        resources.add(new AssetDef<>("anims_item_cocos",
                        dir + "anims_item_cocos.atlas", TextureAtlas.class));
        resources.add(new AssetDef<>("anims_item_cocos_weapon",
                        dir + "anims_item_cocos_weapon.atlas", TextureAtlas.class));
        resources.add(new AssetDef<>("anims_item_totem",
                        dir + "anims_item_totem.atlas", TextureAtlas.class));

        // controls
        dir = "sprites/ui/controls/";
        resources.add(new AssetDef<>("controls",
                        dir + "controls.atlas",
                        TextureAtlas.class));

        dir = "sprites/animations/palm/";
        resources.add(new AssetDef<>("anims_palm", dir + "/anims_palm.atlas",
                        TextureAtlas.class));

        // kawaida animation
        dir = "sprites/animations/characters/";
        resources.add(new AssetDef<>("anims_kawaida",
                        dir + "kawaida/anims_kawaida.atlas",
                        TextureAtlas.class));

        resources.add(new AssetDef<>("anims_colobus",
                        dir + "neutral/colobus/anims_colobus.atlas",
                        TextureAtlas.class));

        // enemy animations
        resources.add(new AssetDef<>("anims_jz_turtle1",
                        dir + "enemies/turtle/anims_jz_turtle1.atlas",
                        TextureAtlas.class));

        resources.add(new AssetDef<>("anims_jz_bird1",
                        dir + "enemies/bird/anims_jz_bird1.atlas",
                        TextureAtlas.class));

        resources.add(new AssetDef<>("anims_jz_trap1",
                        dir + "enemies/trap/anims_jz_trap1.atlas",
                        TextureAtlas.class));

        // avatar icons
        dir = "sprites/icons/avatars/";
        resources.add(new AssetDef<>("icons_avatars",
                        dir + "icons_avatars.atlas",
                        TextureAtlas.class));

        // item sounds
        dir = "sound/collectables/";
        resources.add(new AssetDef<>("fx_item_sun_collected",
                        dir + "fx_item_sun_collected.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_item_banana_collected",
                        dir + "fx_item_banana_collected.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_item_totem_collected",
                        dir + "fx_item_totem_collected.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_item_cocos_collected",
                        dir + "fx_item_cocos_collected.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_item_cocos_hit",
                        dir + "fx_item_cocos_hit.mp3",
                        Sound.class));

        // kawaida sounds
        dir = "sound/kawaida/";
        resources.add(new AssetDef<>("fx_kawaida_walk",
                        dir + "fx_kawaida_walk.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_kawaida_jump",
                        dir + "fx_kawaida_jump.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_kawaida_land_ground",
                        dir + "fx_kawaida_land_ground.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_kawaida_hit",
                        dir + "fx_kawaida_hit.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_kawaida_throw",
                        dir + "fx_kawaida_throw.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_kawaida_die",
                        dir + "fx_kawaida_die.mp3",
                        Sound.class));

        // enemy sounds
        dir = "sound/enemies/";
        resources.add(new AssetDef<>("fx_jz_bird1_fly",
                        dir + "bird/fx_jz_bird1_fly.mp3",
                        Sound.class));

        // game sounds
        dir = "sound/game/";
        resources.add(new AssetDef<>("fx_game_win",
                        dir + "fx_game_win.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_game_lose",
                        dir + "fx_game_lose.mp3",
                        Sound.class));

        dir = "sound/game/";
        resources.add(new AssetDef<>("fx_win1_poa",
                        dir + "fx_win1_poa.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_win2_freshi",
                        dir + "fx_win2_freshi.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_win3_bomba_sana",
                        dir + "fx_win3_bomba_sana.mp3",
                        Sound.class));

        resources.add(new AssetDef<>("fx_plant_a_palm_tree_seed",
                        dir + "fx_plant_a_palm_tree_seed.mp3",
                        Sound.class));

        // music
        String music = map.getProperties().get("music", String.class);

        if (music != null) {
            //            String mPath = "music/levels/" + music + ".mp3";
            String mPath = "music/levels/music_jozani1.mp3";
            //            game.assetMngr.loadMusic(mPath, music);

            resources.add(new AssetDef<>(music, mPath, Music.class));
        }
    }

    private void loadMap() {
        bgMusic = map.getProperties().get("music", String.class);

        tmr.setMap(map);

        Array<TiledMapTileLayer> layers = map.getLayers()
                        .getByType(TiledMapTileLayer.class);

        for (TiledMapTileLayer t : layers) {
            if (t.getName().contains("bg")) bgLayers.add(t);
            else if (t.getName().contains("fg")) fgLayers.add(t);
        }

        sky = ((TiledMapImageLayer) map.getLayers().get("bg#0"))
                        .getTextureRegion().getTexture();
        sky.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);

        horizon = ((TiledMapImageLayer) map.getLayers().get("bg#1"))
                        .getTextureRegion().getTexture();
        horizon.setWrap(TextureWrap.Repeat, TextureWrap.ClampToEdge);

        path = ((TiledMapImageLayer) map.getLayers().get("bg#2"))
                        .getTextureRegion().getTexture();
        path.setWrap(TextureWrap.Repeat, TextureWrap.ClampToEdge);

        foreground = ((TiledMapImageLayer) map.getLayers().get("fg#0"))
                        .getTextureRegion().getTexture();
        foreground.setWrap(TextureWrap.Repeat, TextureWrap.ClampToEdge);

        skyBG = (TiledMapTileLayer) map.getLayers().get("sky");
        horizonBG = (TiledMapTileLayer) map.getLayers().get("horizon");
        pathBG = (TiledMapTileLayer) map.getLayers().get("path");

        terrain = (TiledMapTileLayer) map.getLayers().get("terrain#0");
        collectableLayer = map.getLayers().get("collectables#0");
        enemyLayer = map.getLayers().get("enemies#0");

        lvlWidth = getMapWidth() * getTileWidth();
        lvlHeight = getMapHeight() * getTileHeight();
    }

    public VJXEntity getEntity(long id) {
        return entities.get(id);
    }

    public Saidi saidi;
    PalmTree palm;

    public void restart() {
        clear(true);

        ground = (Ground) game.factory.createEntity("ground");
        addEntity(ground, false);

        palm = (PalmTree) game.factory.createEntity("palm");
        palm.setPosition(14976, 1440);
        addEntity(palm, true);

        Avatar pA = (Avatar) game.factory.createEntity("kawaida");
        addEntity(pA, false);

        pA.setPosition(100, 200);
        pA.getBanana().set(1);
        pA.getTotem().set(0);
        pA.getBody().setLinearVelocity(0, 0);
        pA.setStatus(VJXStatus.IDLE);

        pA.setWeapon((Weapon) game.factory.createEntity("item_cocos_weapon"));
        pA.getWeapon().set(0);

        addEntity(pA.getWeapon(), false);

        saidi = (Saidi) game.factory.createEntity("colobus");
        saidi.setPosition(lvlWidth - 5150, 290);
        addEntity(saidi, true);

        loadObjects();
        loadTerrain();

        music.setVolume(1f);
    }

    private void loadObjects() {
        String name;
        TiledMapTile t;
        VJXEntity object;
        MapProperties mp = map.getProperties();
        TiledMapTileSets tileSets = map.getTileSets();
        MapObjects objects = collectableLayer.getObjects();

        for (MapObject mo : objects) {
            if (mo instanceof TextureMapObject) {

                mp = mo.getProperties();
                t = tileSets.getTile(mp.get("gid", Integer.class));
                name = t.getProperties().get("name", String.class);

                object = game.factory.createEntity(name);
                object.setPosition(mp.get("x", Float.class),
                                mp.get("y", Float.class) + object.getHeight());
                addEntity(object, false);

                if (name.equals("item_sun")) sunCount++;
            }
        }

        objects = enemyLayer.getObjects();
        for (MapObject mo : objects) {
            if (mo instanceof TextureMapObject) {
                mp = mo.getProperties();

                t = tileSets.getTile(mp.get("gid", Integer.class));
                name = t.getProperties().get("name", String.class);

                object = game.factory.createEntity(name);
                if (object instanceof Trap)
                    object.setPosition(mp.get("x", Float.class),
                                mp.get("y", Float.class) + object.getHeight() * 3);
                else
                    object.setPosition(mp.get("x", Float.class),
                                mp.get("y", Float.class) + object.getHeight());
                addEntity(object, false);
            }
        }
    }

    private void loadTerrain() {
        String name;
        //        TiledMapTile t;

        //        MapProperties mp = map.getProperties();
        //        TiledMapTileSets tileSets = map.getTileSets();
        //        MapObjects objects = terrain.getObjects();

        Cell c;
        boolean[][] checked = new boolean[terrain.getWidth()][terrain.getHeight()];
        for (int n = 0; n < terrain.getWidth(); n++) {
            for (int m = 0; m < terrain.getHeight(); m++) {
                if (checked[n][m]) continue;
                checked[n][m] = true;

                c = terrain.getCell(n, m);

                if (c == null) continue;

                name = c.getTile().getProperties().get("name", String.class);

                Array<Vector2> vertices = new Array<>();
                if (name == null) continue;
                if (name.contains("platform")) {
                    if (name.contains("left")) {
                        vertices.add(new Vector2(n * getTileWidth() + 28,
                                        m * getTileHeight() + 100));
                        //                        vertices.add(new Vector2(n * getTileWidth() + 64,
                        //                                        m * getTileHeight() + 100));
                        //                        vertices.add(new Vector2(n * getTileWidth() + 128,
                        //                                        m * getTileHeight() + 100));
                        float x = n * getTileWidth() + 128;

                        int tmpN = n + 1;
                        c = terrain.getCell(tmpN, m);
                        while (c != null) {
                            name = c.getTile().getProperties().get("name",
                                            String.class);
                            if (name != null)
                            if (name.contains("platform")) {
                                checked[tmpN++][m] = true;
                                if (name.contains("mid")) {
                                    x += 128;
                                    //                                    vertices.add(new Vector2(
                                    //                                                    tmpN * getTileWidth() + 64,
                                    //                                                    m * getTileHeight() + 100));
                                    //                                    vertices.add(new Vector2(
                                    //                                                    tmpN++ * getTileWidth() + 128,
                                    //                                                    m * getTileHeight() + 100));
                                    c = terrain.getCell(tmpN, m);
                                }
                                if (name.contains("right")) {
                                    x += 90;
                                    //                                    vertices.add(new Vector2(
                                    //                                                    tmpN * getTileWidth() + 64,
                                    //                                                    m * getTileHeight() + 100));
                                    //                                    vertices.add(new Vector2(
                                    //                                                    tmpN++ * getTileWidth() + 90,
                                    //                                                    m * getTileHeight() + 100));
                                    c = null;
                                }
                            }
                        }
                        vertices.add(new Vector2(x,
                                        m * getTileHeight() + 100));

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        ground.addPlatform(v);
                    }
                    continue;
                }
                if (name.contains("tree")) {
                    if (name.contains("crown_top")) {
                        vertices.add(new Vector2(n * getTileWidth() - 85,
                                        m * getTileHeight() + 32));
                        vertices.add(new Vector2((n + 1) * getTileWidth() + 85,
                                        m * getTileHeight() + 32));

                        if (n - 1 >= 0) checked[n - 1][m] = true;
                        if (n + 1 < getMapWidth()) checked[n + 1][m] = true;

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        ground.addPlatform(v);
                        continue;
                    }
                    if (name.contains("arm_left")) {
                        vertices.add(new Vector2(n * getTileWidth() + 10,
                                        m * getTileHeight() + 110));
                        vertices.add(new Vector2((n + 1) * getTileWidth() - 10,
                                        m * getTileHeight() + 110));

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        ground.addPlatform(v);
                        continue;
                    }
                    if (name.contains("arm_right")) {
                        vertices.add(new Vector2(n * getTileWidth() + 10,
                                        m * getTileHeight() + 95));
                        vertices.add(new Vector2((n + 1) * getTileWidth() - 10,
                                        m * getTileHeight() + 95));

                        Vector2[] v = new Vector2[vertices.size];
                        for (int i = 0; i < vertices.size; i++) {
                            v[i] = vertices.get(i).scl(1 / 100f);
                        }
                        ground.addPlatform(v);
                        continue;
                    }
                }
            }
        }
    }

    //    private float timePassed = 0f;
    //    private static float step = 1f / 60f;

    @Override
    protected void init() {
        tmr = new OrthogonalTiledMapRenderer(new TiledMap(), getBatch());

        b2dWorld = new World(new Vector2(0f, -9.81f), true);
        b2dWorld.setContactListener(game.cCtrl);

        game.dbgStage.setB2DWorld(b2dWorld);

        addActor(gameWorldNode);

        loadMap();

        music = game.assetMngr.getMusic(bgMusic);

        restart();
    }

    @Override
    public void preAct(float delta) {
        //        timePassed += delta;

        //        if (timePassed >= step) {
        b2dWorld.step(delta, 6, 2);
        //            timePassed -= step;
        //        }
    }


    @Override
    public void postAct(float delta) {
        for (VJXEntity e : entities.values()) {
            if (e.getCategory() == VJXPhys.REMOVE.id) {
                e.getBody().setType(BodyType.DynamicBody);
                if (e.getY() < -200) markRemove(e);
            }
        }
    }

    private ParallaxCamera paraCam;
    private Matrix4 m = new Matrix4();

    private final Vector3 curr = new Vector3();
    private final Vector3 last = new Vector3(-1, -1, -1);
    private final Vector3 delta = new Vector3();
    private float paraX = 0;

    @Override
    public void preDraw() {
        getBatch().begin();

        m.set(getBatch().getProjectionMatrix());

        drawBackground();

        for (TiledMapTileLayer t : bgLayers)
            tmr.renderTileLayer(t);

        tmr.renderTileLayer(terrain);

        //        getBatch().setTransformMatrix(new Matrix4());
        getBatch().setProjectionMatrix(m);

        getBatch().end();
    }

    private void drawBackground() {
        Vector2 v = new Vector2(player.getAvatar().getCenter());
        v.x = v.x > game.targetRes.x / 2 ? v.x : game.targetRes.x / 2;
        v.y = v.y > game.targetRes.y / 2 ? v.y : game.targetRes.y / 2;
        v.scl(.01f);

        game.getB2DCam().position.set(v, game.getB2DCam().position.z);
        game.getB2DCam().update();

        cam.position.set(v.scl(100), cam.position.z);
        cam.update();

        paraCam.position.set(cam.position);
        //        paraCam.update();

        paraCam.unproject(curr.set(cam.position.x, cam.position.y, 0));
        if (!(last.x == -1 && last.y == -1 && last.z == -1)) {
            paraCam.unproject(delta.set(last.x, last.y, 0));
            delta.sub(curr);
            paraCam.position.add(delta.x, 0, 0);
        }
        last.set(cam.position.x, cam.position.y, 0);

        Matrix4 trans = new Matrix4();

        Vector3 viewSize = new Vector3(cam.viewportWidth * cam.zoom,
                        cam.viewportWidth * cam.zoom, 0);

        Vector3 pos = new Vector3(cam.position.x - viewSize.x / 2f,
                        cam.position.y - viewSize.y / 2f, 0);

        trans = new Matrix4();
        paraX = -(getWidth() - getWidth() * .8f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * .8f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(.8f, 1));
        tmr.renderTileLayer(skyBG);

        trans.idt();
        paraX = -(getWidth() - getWidth() * .9f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * .9f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(.9f, 1));
        tmr.renderTileLayer(horizonBG);

        trans.idt();
        paraX = -(getWidth() - getWidth() * 1f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(paraCam.calculateParallaxMatrix(1f, 1));
        tmr.renderTileLayer(pathBG);
    }

    @Override
    public void postDraw() {
        getBatch().begin();

        m.set(getBatch().getProjectionMatrix());

        Matrix4 trans = new Matrix4();

        Vector3 viewSize = new Vector3(cam.viewportWidth * cam.zoom,
                        cam.viewportWidth * cam.zoom, 0);

        Vector3 pos = new Vector3(cam.position.x - viewSize.x / 2f,
                        cam.position.y - viewSize.y / 2f, 0);

        paraX = -(getWidth() - getWidth() * 1.17f) / 2;
        trans.translate(paraX, 0, 0);
        tmr.setView(cam.combined, pos.x * 1.17f, pos.y, viewSize.x, viewSize.y);
        getBatch().setTransformMatrix(trans);
        getBatch().setProjectionMatrix(
                        paraCam.calculateParallaxMatrix(1.17f, 1));

        for (TiledMapTileLayer t : fgLayers)
            tmr.renderTileLayer(t);

        //        getBatch().setTransformMatrix(new Matrix4());
        getBatch().setProjectionMatrix(m);

        getBatch().end();
        clear(false);
    }

    public void addEntity(VJXEntity entity, boolean addRoot) {

        if (!addRoot) {
            super.registerEntity(entity);

            if (entity instanceof Enemy) {
                enemiesNode.addActor(entity);
            }
            if (entity instanceof Avatar) {
                charactersNode.addActor(entity);
            }
            if (entity instanceof Valuable) {
                valuablesNode.addActor(entity);
            }
            if (entity instanceof Consumable) {
                consumableNode.addActor(entity);
            }
            if (entity instanceof Weapon) {
                weaponsNode.addActor(entity);
            }
            if (entity instanceof Ground) {
                groundNode.addActor(entity);
            }
            if (entity instanceof Obstacle) {
                obstaclesNode.addActor(entity);
            }
        } else super.addActor(entity);

        entity.spawnBody(b2dWorld);
    }

    public void markRemove(VJXEntity entity) {
        if (entity == null) return;
        removeIPs.add(entity.getID());
        if (entity.getName().equals("item_sun")) sunCount--;
    }

    private void clear(boolean all) {
        if (all) {
            for (VJXEntity e : entities.values()) {
                markRemove(e);
            }
        }

        long id = -1;
        while (removeIPs.size() > 0) {
            id = removeIPs.removeFirst();

            game.factory.destroy(id);
            VJXEntity e = entities.get(id);
            e.destroyBody();

            unregisterEntity(id);
        }
    }

    public int getTileWidth() {
        return map.getProperties().get("tilewidth", Integer.class);
    }

    public int getTileHeight() {
        return map.getProperties().get("tileheight", Integer.class);
    }

    public int getMapWidth() {
        return map.getProperties().get("width", Integer.class);
    }

    public int getMapHeight() {
        return map.getProperties().get("height", Integer.class);
    }

    public float getLvlWidth() {
        return lvlWidth;
    }

    public float getLvlHeight() {
        return lvlHeight;
    }

    public String getBgMusicName() {
        return bgMusic;
    }

    public World getB2DWorld() {
        return b2dWorld;
    }

    public int sunCount() {
        return sunCount;
    }

    @Override
    public void dispose() {
        game.assetMngr.disposeMap(getName());

        bgLayers.clear();
        fgLayers.clear();

        clear(true);
        if (b2dWorld != null)
        b2dWorld.dispose();
        super.dispose();
    }
}