package de.kawaida.kjgame.entities.neutral;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.kjgame.entities.actors.Neutral;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.venjinx.core.libgdx.entity.VJXEntity;

public class Saidi extends Neutral {

    public Saidi() {
        super(BodyType.StaticBody);
        setName("colobus");
    }

    @Override
    public void visit(Acceptor vjxE) {
        // TODO Auto-generated method stub

    }

    @Override
    public void hit(Acceptor vjxE) {
        // TODO Auto-generated method stub

    }

    @Override
    public void trigger(Acceptor vjxE) {
        // TODO Auto-generated method stub

    }

    @Override
    public void collideWith(VJXEntity other) {
        // TODO Auto-generated method stub

    }

    @Override
    public void trigger(Fixture triggerFixture) {
        // TODO Auto-generated method stub

    }

    @Override
    public void userAct(float deltaT) {
    }

    @Override
    public void userDraw(Batch batch, float parentAlpha) {
        // TODO Auto-generated method stub

    }

    @Override
    public void spawnActor(World world) {
        // TODO Auto-generated method stub

    }
}