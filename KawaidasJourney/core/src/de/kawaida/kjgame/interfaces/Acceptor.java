package de.kawaida.kjgame.interfaces;

public interface Acceptor {

    public void accVisitor(Visitor visitor);

    public void accCollision(Visitor visitor);

    public void accTrigger(Visitor visitor);

}
