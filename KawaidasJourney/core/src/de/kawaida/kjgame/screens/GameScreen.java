package de.kawaida.kjgame.screens;

import de.kawaida.kjgame.entities.level.KJLevel;
import de.kawaida.kjgame.stages.IngameHUD;
import de.kawaida.kjgame.stages.IntroStage;
import de.kawaida.kjgame.stages.Menu;
import de.venjinx.core.libgdx.EntityListener;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXScreen;

public class GameScreen extends VJXScreen {

    public EntityListener entityListener;

    private Menu menu;
    private KJLevel level;
    private IntroStage intro;
    //    private KJLevel levelIntro;
    private IngameHUD ingameHUD;

    public GameScreen(VJXGame game) {
        super(game);

        menu = new Menu(game);
        intro = new IntroStage(game);
        level = new KJLevel(game);
        //        levelIntro = new KJLevel(game);
        ingameHUD = new IngameHUD(game);

        entityListener = new EntityListener(this);
    }

    @Override
    protected void userUpdate(float deltaT) {
    }

    @Override
    protected void userRender(float deltaT) {
    }

    public void loadMenu(String name) {
        removeStage(intro);
        removeStage(level);
        removeStage(ingameHUD);

        if (name != null && !name.equals("")) menu.setEntryMenu(name);

        addStage(menu);

        game.getView().update((int) game.res.x, (int) game.res.y, true);

        loader.processNext();
    }

    public void loadIntro() {
        removeStage(menu);

        addStage(intro);

        game.getView().update((int) game.res.x, (int) game.res.y, true);

        loader.processNext();
    }

    public void loadLevel(String path, boolean internal) {
        removeStage(intro);

        level.setPath(path, internal);
        addStage(level);

        addStage(ingameHUD);

        loader.processNext();
    }

    public void startGame() {

    }

    @Override
    public void restart() {
        level.restart();
        //        levelIntro.restart();

        game.getScreen().getHUD().updateHUD();

        resume();
    }

    public KJLevel getLevel() {
        return level;
    }

    public IngameHUD getHUD() {
        return ingameHUD;
    }

    @Override
    public void dispose() {
        removeStage(level);
        //        removeStage(levelIntro);
        removeStage(ingameHUD);
        if (level != null) level.dispose();
        //        if (levelIntro != null) levelIntro.dispose();
        if (ingameHUD != null) ingameHUD.dispose();
    }

    @Override
    public void hide() {
    }
}