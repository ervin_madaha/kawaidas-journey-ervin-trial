package de.kawaida.kjgame.stages;

import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;

public class EndStage extends VJXStage {

    public EndStage(VJXGame game) {
        super(game);
    }

    @Override
    protected void init() {

    }

    @Override
    public void preAct(float delta) {

    }

    @Override
    public void postAct(float delta) {

    }

    @Override
    public void preDraw() {

    }

    @Override
    public void postDraw() {

    }

}
