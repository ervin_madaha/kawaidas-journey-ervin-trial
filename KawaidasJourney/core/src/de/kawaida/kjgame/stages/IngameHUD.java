package de.kawaida.kjgame.stages;

import java.util.ArrayDeque;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.ui.IngameMenu;
import de.kawaida.ui.LoseMenu;
import de.kawaida.ui.PauseMenu;
import de.kawaida.ui.ProjectsPopupMenu;
import de.kawaida.ui.SettingsMenu;
import de.kawaida.ui.ShopMenu;
import de.kawaida.ui.ShopPrimeMenu;
import de.kawaida.ui.StoryDialog;
import de.kawaida.ui.UiTable;
import de.kawaida.ui.UserInterface;
import de.kawaida.ui.WinMenu;
import de.kawaida.ui.skins.InterfaceSkin;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.VJXTypes.VJXDirection;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.entity.VJXEntity;

public class IngameHUD extends VJXStage implements EventListener {

    //    private Sprite current;

    Actor mapActor = new Actor();
    Actor restart = new Actor();
    Actor saveMe = new Actor();
    Actor shop = new Actor();
    Actor giveUp = new Actor();
    Actor buyTotem = new Actor();

    private InterfaceSkin hudSkin;

    private UserInterface uHUD;
    private IngameMenu ingameMenu;

    private PauseMenu pauseMenu;
    private WinMenu winMenu;
    private LoseMenu loseMenu;
    private ProjectsPopupMenu projectsPopup;
    private SettingsMenu settingsMenu;
    private ShopMenu shopMenu;
    private ShopPrimeMenu shopPrimeMenu;

    private boolean jumpPressed = false;
    private float chargeTime = 0f;

    private Sprite objective;

    public IngameHUD(VJXGame game) {
        super(game);
        setName("ingameHUD");

        setViewport(game.getInterfaceView());

        ingameMenu = new IngameMenu(game.menuSkin);

//        resources.add(new AssetDef<>("menu_", "menu/menu_pause.jpg",
        //                        Texture.class));

        resources.add(new AssetDef<>("dialog0", "menu/dialog/dialog0.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("dialog1", "menu/dialog/dialog1.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("dialog2", "menu/dialog/dialog2.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("dialog3", "menu/dialog/dialog3.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_win", "menu/menu_win.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_lose", "menu/menu_lose.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_pause", "menu/menu_pause.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("plant_your_palm",
                        "menu/plant_your_palm.png",
                        Texture.class));

        resources.add(new AssetDef<>("menu_settings",
                        "menu/settings/menu_settings.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_projects_popup",
                        "menu/projects/menu_projects_popup.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_shop", "menu/shop/menu_shop.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_shop_prime",
                        "menu/shop/menu_shop_prime.jpg", Texture.class));
    }

    //    boolean draw = false;

    public void pause(boolean pause) {
        if (pause) {
            game.getScreen().pause();
            ingameControl = false;
            game.assetMngr.getMusic(
                            game.getScreen().getLevel().getBgMusicName())
                            .setVolume(.3f);
        } else {
            ingameMenu.remove();
            addActor(uHUD);

            uHUD.getJoystick().addListener(this);
            addActor(uHUD.getJoystick());

            game.getScreen().resume();
            ingameControl = true;
            game.assetMngr.getMusic(
                            game.getScreen().getLevel().getBgMusicName())
                            .setVolume(1f);
        }
    }

    public void showDialog(VJXEntity leftEntity, VJXEntity rightEntity) {
        clear();
        uHUD.getJoystick().remove();

        StoryDialog sd = ingameMenu.getStoryDialog();

        ArrayDeque<SpriteDrawable> parts = new ArrayDeque<>();
        parts.add(game.assetMngr.createSpriteDrawable("dialog0"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog1"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog2"));
        parts.add(game.assetMngr.createSpriteDrawable("dialog3"));

        sd.setDialogParts(parts);

        ingameMenu.prepare();
        ingameMenu.setBackground(sd.nextSprite());
        addActor(ingameMenu);

        game.getScreen().pause();
        ingameControl = false;
    }

    private boolean gameOver = false;

    public void outro() {
        gameOver = true;

        ingameMenu.remove();
        game.getScreen().resume();

        Vector2 bodyVel = new Vector2();
        bodyVel.x = game.getPlayer().getAvatar().getSpeed() / 2f;
        player.getAvatar().setDirection(VJXDirection.Right);
        player.getAvatar().setStatus(VJXStatus.WALK);
        player.getAvatar().playSound("fx_kawaida_walk", true);
        player.getAvatar().getBody().setLinearVelocity(bodyVel);
        ingameControl = false;
    }

    public void showProject() {
        clear();
        uHUD.getJoystick().remove();

        projectsPopup.prepare();
        addActor(projectsPopup);

        game.getScreen().pause();
        ingameControl = false;

        last = null;
    }

    public void showPause() {
        pause(true);
        clear();
        uHUD.getJoystick().remove();

        pauseMenu.prepare();
        addActor(pauseMenu);

        player.getAvatar().stopAllSounds();
        ingameControl = false;
    }

    private boolean won = false;

    public void win() {
        clear();
        uHUD.getJoystick().remove();

        winMenu.prepare();
        addActor(winMenu);

        game.assetMngr.getMusic(game.getScreen().getLevel().getBgMusicName())
                        .stop();

        if (!won) {
            game.assetMngr.getSound("fx_game_win").play();
            time = 0;
            c = 0;
        }

        //        game.getScreen().pause();
        player.getAvatar().stopAllSounds();
        ingameControl = false;
        won = true;
        last = winMenu;
    }

    public void lose() {
        clear();
        uHUD.getJoystick().remove();

        loseMenu.prepare();
        addActor(loseMenu);

        game.assetMngr.getMusic(game.getScreen().getLevel().getBgMusicName())
                        .stop();
        game.assetMngr.getSound("fx_game_lose").play();

        game.getScreen().pause();
        player.getAvatar().stopAllSounds();
        ingameControl = false;
        last = loseMenu;
    }

    @Override
    protected void init() {
        ArrayList<TextureAtlas> tas = new ArrayList<>();
        tas.add(game.assetMngr.getSpriteSheet("controls"));
        tas.add(game.assetMngr.getSpriteSheet("collectables"));

        hudSkin = new InterfaceSkin(tas, game.font);

        uHUD = new UserInterface(hudSkin, "ui_main");
        addActor(uHUD);

        uHUD.getJoystick().addListener(this);
        addActor(uHUD.getJoystick());

        uHUD.update(player);

        giveUp.setName("ui_btn_giveUp");
        //        giveUp.setPosition(250, 50);
        //        giveUp.setSize(230, 75);

        restart.setName("ui_btn_restart");
        //        restart.setPosition(500, 5);
        //        restart.setSize(230, 75);

        mapActor.setName("ui_btn_map");
        //        mapActor.setPosition(250, 50);
        //        mapActor.setSize(230, 75);

        buyTotem.setName("ui_btn_buy");
        //        buyTotem.setPosition(400, 460);
        //        buyTotem.setSize(100, 75);

        shop.setName("ui_btn_shop");
        //        shop.setPosition(730, 460);
        //        shop.setSize(230, 75);

        pauseMenu = new PauseMenu(game.menuSkin);
        pauseMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_pause"));

        winMenu = new WinMenu(game.menuSkin);
        winMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_win"));

        loseMenu = new LoseMenu(game.menuSkin);
        loseMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_lose"));

        shopMenu = new ShopMenu(game.menuSkin);
        shopMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_shop"));

        shopPrimeMenu = new ShopPrimeMenu(game.menuSkin);
        shopPrimeMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_shop_prime"));

        settingsMenu = new SettingsMenu(game.menuSkin);
        settingsMenu.setBackground(
                        game.assetMngr.createSpriteDrawable("menu_settings"));

        projectsPopup = new ProjectsPopupMenu(game.menuSkin);
        projectsPopup.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_projects_popup"));

        objective = game.assetMngr.createSprite("plant_your_palm");
        objective.setPosition(500, 400);

        gameOver = false;
    }

    float time = 0;

    Sound s;

    int c = -1;

    @Override
    public void preAct(float delta) {
        if (first) game.assetMngr.getSound("fx_plant_a_palm_tree_seed").play();
        time += delta;

        if (time > 1 && c == 0) {
            game.assetMngr.getSound("fx_win1_poa").play();
            c++;
        }

        if (time > 2 && c == 1) {
            game.assetMngr.getSound("fx_win2_freshi").play();
            c++;
        }

        if (time > 3 && c == 2) {
            game.assetMngr.getSound("fx_win3_bomba_sana").play();
            c++;
        }

        if (!ingameControl || player.getAvatar().isDead()) return;

        if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.D)) {
            if (player.getAvatar().getStatus() == VJXStatus.HIT
                            || player.getAvatar().getStatus() == VJXStatus.DIE)
                return;
            if (Gdx.input.isKeyPressed(Keys.A)) {
                player.getAvatar().setDirection(VJXDirection.Left);
            }
            if (Gdx.input.isKeyPressed(Keys.D)) {
                player.getAvatar().setDirection(VJXDirection.Right);
            }
            if (!jumpPressed)
            player.getAvatar().run();
        }
    }

    @Override
    public void postAct(float delta) {
        if (player.getAvatar().isDead()) lose();

        if (jumpPressed) {
            chargeTime += delta;

            Vector2 bodyVel = player.getAvatar().getBody().getLinearVelocity();
            bodyVel.y = player.getAvatar().getJumpHeight();
            player.getAvatar().getBody().setLinearVelocity(bodyVel);

            if (chargeTime >= .5f) {
                jumpPressed = false;
                chargeTime = 0;
            }
        }

        Avatar pA = player.getAvatar();

        if (pA.getCenter().x > game.getScreen().getLevel().getLvlWidth() - 5400
                        && !gameOver) {
            showDialog(pA, game.getScreen().getLevel().saidi);
            player.getAvatar().stopAllSounds();
        }

        if (gameOver) {
            player.getAvatar().setStatus(VJXStatus.WALK);
            if (pA.getCenter().x > game.getScreen().getLevel().getLvlWidth()
                            - 4300)
                win();
        }
        first = false;
    }

    private boolean first = true;

    @Override
    public void preDraw() {
        Vector2 v = new Vector2(game.getPlayer().getAvatar().getCenter());
        v.x = v.x > game.targetRes.x / 2 ? v.x : game.targetRes.x / 2;
        v.y = v.y > game.targetRes.y / 2 ? v.y : game.targetRes.y / 2;
        v.scl(.01f);
        game.getB2DCam().position.set(v, game.getB2DCam().position.z);
        game.getB2DCam().update();

        game.getCam().position.set(v.scl(100), game.getCam().position.z);
        game.getCam().update();

        //        if (draw) {
        //            getBatch().begin();
        //            current.setCenter(game.getCam().position.x,
        //                            game.getCam().position.y);
        //            current.draw(getBatch());
        //            getBatch().end();
        //        }
    }

    @Override
    public void postDraw() {

        if (time < 2 && !gameOver) {
            getBatch().begin();
            objective.draw(getBatch());
            getBatch().end();
        } else {

            if (!game.assetMngr.getMusic(
                            game.getScreen().getLevel().getBgMusicName())
                            .isPlaying() && last != loseMenu && last != winMenu)
                game.assetMngr.getMusic(
                                game.getScreen().getLevel().getBgMusicName())
                                .play();
        }
    }

    private boolean cancel() {
        if (!player.getAvatar().isSpawned()
                        || player.getAvatar().getStatus().blocking)
            return true;

        return false;
    }

    @Override
    public boolean handle(Event event) {
        Actor actor = event.getTarget();
        //                    System.out.println("------------Handle change event----------");
        //                    System.out.println("Event      : " + event);
        //                    System.out.println("Actor      : " + actor.getName());
        //                    System.out.println("EventActor : " + eActor.getName());
        //                    System.out.println("EventTarget: " + target.getName());
        //                    System.out.println("-----------------------------------------");
        //                    System.out.println();

        if (cancel() || actor == null) return true;

        if (actor.getName().contains("joystick")) {
            Touchpad tp = (Touchpad) actor;

            float knobX = tp.getKnobPercentX();

            if (knobX < 0)
                player.getAvatar().setDirection(VJXDirection.Left);
            if (knobX > 0)
                player.getAvatar().setDirection(VJXDirection.Right);

            if (player.getAvatar().isOnGround()) {
                if (knobX == 0) {
                    player.getAvatar().stop();
                } else {
                    player.getAvatar().run();
                }
            }

            Body body = player.getAvatar().getBody();
            Vector2 bodyVel = body.getLinearVelocity();

            bodyVel.x = knobX * player.getAvatar().getSpeed();
            body.setLinearVelocity(bodyVel);
        }
        return true;
    }

    private boolean ingameControl = true;

    @Override
    public boolean keyDown(int keycode) {

        if (ingameControl) {

            if (keycode == Keys.SPACE) if (player.getAvatar().isOnGround()) {
                jumpPressed = true;
                player.getAvatar().jump();
                return true;
            }

            if (keycode == Keys.M) {
                player.getAvatar().throwCocos();
                uHUD.update(player);
                return true;
            }

            if (keycode == Keys.NUM_1) {
                player.getAvatar().setStatus(VJXStatus.IDLE);
                return true;
            }

            if (keycode == Keys.NUM_2) {
                player.getAvatar().setStatus(VJXStatus.HIT);
                return true;
            }

            if (keycode == Keys.NUM_3) {
                player.getAvatar().setStatus(VJXStatus.RUN);
                return true;
            }

            if (keycode == Keys.NUM_4) {
                player.getAvatar().setStatus(VJXStatus.DIE);
                return true;
            }

            if (keycode == Keys.NUM_5) {
                player.getAvatar().setStatus(VJXStatus.JUMP);
                return true;
            }

            if (keycode == Keys.NUM_6) {
                player.getAvatar().setStatus(VJXStatus.THROW);
                return true;
            }

            if (keycode == Keys.NUM_7) {
                player.getAvatar().setStatus(VJXStatus.WALK);
                return true;
            }
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (ingameControl) {
            if (keycode == Keys.A) {
                player.getAvatar().stop();
                return true;
            }

            if (keycode == Keys.D) {
                player.getAvatar().stop();
                return true;
            }
            if (keycode == Keys.SPACE) {
                if (jumpPressed) jumpPressed = false;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (ingameControl) {
            if (gameOver) return true;

            Vector2 v;
            screenToStageCoordinates(v = new Vector2(screenX, screenY));
            Actor a = hit(v.x, v.y, true);

            if (a == null) {

                if (v.x < getWidth() / 2) {
                    uHUD.setJoysticPos(v.x, v.y);
                    return super.touchDown(screenX, screenY, pointer, button);
                } else if (player.getAvatar().isOnGround()) {
                    jumpPressed = true;
                    player.getAvatar().jump();
                    return true;
                }
                //                return true;
            }

            if (a == null) return true;

            if (a.getName().contains("joystick")) {
                return super.touchDown(screenX, screenY, pointer, button);
            }

            //            if (v.x < getWidth() / 2) {
            //                uHUD.setJoysticPos(v.x, v.y);
            //                return super.touchDown(screenX, screenY, pointer, button);
            //            } else if (a.getName().equals("ui_btn_cocos"))
            //                if (player.getAvatar().isOnGround()) {
            //                jumpPressed = true;
            //                player.getAvatar().jump();
            //                return true;
            //            }

            if (a.getName().equals("ui_btn_pause")) { return true; }

            if (button == 1) {
                player.getAvatar().throwCocos();
                uHUD.update(player);
                return true;
            }
        }

        return false;
    }

    private UiTable last;

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector2 v;
        screenToStageCoordinates(v = new Vector2(screenX, screenY));

        Actor a = hit(v.x, v.y, true);

        if (a == null) return false;

        if (ingameControl) {

            if (a.getName().equals("ui_btn_pause")) {
                showPause();
                return true;
            }

            if (a.getName().equals("ui_btn_cocos")) {
                player.getAvatar().throwCocos();
                uHUD.update(player);
                return true;
            }

            if (v.x < getWidth() / 2) {
                super.touchUp(screenX, screenY, pointer, button);
            } else if (jumpPressed && !gameOver) {
                jumpPressed = false;
                return true;
            }
        } else {
            if (a.getName().equals("ui_btn_restart")) {
                if (player.getAvatar().isDead() && player.getAvatar().getTotem()
                                .getAmount() < 1) { return true; }
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());

                uHUD.getJoystick().addListener(this);

                //            draw = false;
                game.getScreen().restart();
                ingameControl = true;
                gameOver = false;
                return true;
            }

            if (a.getName().equals("ui_btn_map_znz")
                            || a.getName().equals("ui_btn_map")) {
                clear();
                //            game.assetMngr.getMusic(
                //                            game.getScreen().getLevel().getBgMusicName())
                //                            .stop();
                game.getScreen().loadMenu(a.getName());
                //                pause(false);
                return true;
            }

            if (a.getName().equals("ui_btn_resume")) {
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());

                uHUD.getJoystick().addListener(this);

                pause(false);
                return true;
            }

            if (a.getName().equals("ui_btn_close")) {
                clear();
                addActor(uHUD);
                addActor(uHUD.getJoystick());

                uHUD.getJoystick().addListener(this);

                pause(false);
                return true;
            }

            if (a.getName().equals("ui_btn_settings")) {
                clear();
                //                uHUD.getJoystick().remove();

                settingsMenu.prepare();
                addActor(settingsMenu);

                //                game.getScreen().pause();
                //                ingameControl = false;
                last = pauseMenu;
                return true;
            }

            if (a.getName().equals("ui_btn_shop")) {
                clear();
                //                uHUD.getJoystick().remove();

                shopMenu.prepare();
                addActor(shopMenu);

                game.getScreen().pause();
                //                ingameControl = false;
                return true;
            }

            if (a.getName().equals("ui_btn_shop_prime")) {
                clear();
                //                uHUD.getJoystick().remove();

                shopPrimeMenu.prepare();
                addActor(shopPrimeMenu);

                //                game.getScreen().pause();
                //                ingameControl = false;
                return true;
            }

            if (a.getName().equals("ui_btn_back")) {
                shopMenu.remove();
                shopPrimeMenu.remove();
                settingsMenu.remove();
                projectsPopup.remove();

                if (last != null) {
                    last.prepare();
                    addActor(last);
                } else {
                    addActor(uHUD);
                    addActor(uHUD.getJoystick());

                    uHUD.getJoystick().addListener(this);
                    game.getScreen().resume();
                    ingameControl = true;
                }
                return true;
            }

            if (a.getName().equals("ui_btn_next")) {
                SpriteDrawable s = ingameMenu.getStoryDialog().nextSprite();
                ingameMenu.setBackground(s);

                if (s == null)
                    outro();
                return true;
            }

            if (a.getName().equals("ui_btn_exit")) {
                Gdx.app.exit();
            }
        }

        return false;
    }

    public void updateHUD() {
        uHUD.update(player);
    }
}