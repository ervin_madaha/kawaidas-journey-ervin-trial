package de.kawaida.kjgame.stages;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import de.kawaida.ui.widgets.KJLabel;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.assets.AssetDef;

public class IntroStage extends VJXStage {

    private Sprite currentFrame;
    private int currentFrameNr = 0;
    private float timer = 0;
    private KJLabel infoLabel;

    private int imgCount = 600;
    private Music music;

    public IntroStage(VJXGame game) {
        super(game);

        for (int i = 0; i < imgCount; i += 3) {

            resources.add(new AssetDef<>("intro" + i,
                            "video/jozani0/intro_jozani0#" + i + ".jpg",
                            Texture.class));

        }

        String mPath = "music/levels/intro.mp3";
        resources.add(new AssetDef<>("intro", mPath, Music.class));
    }

    @Override
    protected void init() {
        currentFrameNr = 0;
        currentFrame = game.assetMngr.createSprite("intro" + currentFrameNr);
        timer = 0;

        infoLabel = new KJLabel("Click to skip", game.menuSkin);
        infoLabel.setPosition(1100, -100);

        music = game.assetMngr.getMusic("intro");
    }

    private float step = 1 / 12f;

    @Override
    public void preAct(float delta) {
        timer += delta;

        if (!music.isPlaying()) music.play();

        if (timer >= step) {
            if (currentFrameNr < imgCount) {
                //            System.out.println("step");
                currentFrame.setTexture(
                            game.assetMngr.getTexture("intro" + currentFrameNr));
                currentFrameNr += 3;
            } else {
                step = 1 / 5;
                music.setVolume(music.getVolume() - .01f);
            }
            timer -= step;
        }

        if (currentFrameNr == imgCount) {
            infoLabel.setText("Click to start");
            if (music.getVolume() <= 0) music.stop();
        }
    }

    @Override
    public void postAct(float delta) {

    }

    @Override
    public void preDraw() {

    }

    @Override
    public void postDraw() {
        getBatch().begin();
        currentFrame.draw(getBatch());
        //        if (currentFrameNr == imgCount)
        infoLabel.draw(getBatch(), 1);
        getBatch().end();
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (currentFrameNr > 10 && button == 0) {
            String path;
            //            String name = mapMenu.getLevelName();
            //                path = "levels/" + name.split("#")[0] + "/" + name + ".tmx";
            path = "levels/jozani/jozani_prototype#03.tmx";
            //            game.getScreen().loadIntro();
            game.getScreen().loadLevel(path, true);
            music.stop();
        }

        return true;
    }

}
