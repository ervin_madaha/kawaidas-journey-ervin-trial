package de.kawaida.kjgame.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.kawaida.ui.EncyMenu;
import de.kawaida.ui.EncyPlantsMenu;
import de.kawaida.ui.MapTZMenu;
import de.kawaida.ui.MapZNZMenu;
import de.kawaida.ui.ProjectsMenu;
import de.kawaida.ui.ProjectsPopupMenu;
import de.kawaida.ui.SettingsMenu;
import de.kawaida.ui.ShopMenu;
import de.kawaida.ui.ShopPrimeMenu;
import de.kawaida.ui.SoundtrackMenu;
import de.kawaida.ui.StartMenu;
import de.kawaida.ui.UiTable;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.assets.AssetDef;

public class Menu extends VJXStage {

    private String nextMenu;
    private UiTable currentMenu;

    private StartMenu startMenu;

    private ProjectsMenu projectsMenu;
    private ProjectsPopupMenu projectsPopup;

    private MapTZMenu mapTZMenu;
    private MapZNZMenu mapZNZMenu;

    private SettingsMenu settingsMenu;

    private EncyMenu encyMenu;
    private EncyPlantsMenu encyPlantsMenu;

    private ShopMenu shopMenu;
    private ShopPrimeMenu shopPrimeMenu;

    private SoundtrackMenu soundtrackMenu;

    private boolean mouseDown = false;

    public Menu(VJXGame game) {
        super(game);

        setName("menu");

        //        resources.add(new AssetDef<>("menu_logo", "menu/start/menu_logo.png",
        //                        Texture.class));

        resources.add(new AssetDef<>("menu_start",
                        "menu/start/menu_start.jpg", Texture.class));

        //        resources.add(new AssetDef<>("ui_btn_fb_down",
        //                        "menu/buttons/ui_btn_fb_down.png", Texture.class));
        //        resources.add(new AssetDef<>("ui_btn_fb_up",
        //                        "menu/buttons/ui_btn_fb_up.png", Texture.class));

        //        resources.add(new AssetDef<>("ui_btn_start_down",
        //                        "menu/buttons/ui_btn_start_down.png", Texture.class));
        //        resources.add(new AssetDef<>("ui_btn_start_up",
        //                        "menu/buttons/ui_btn_start_up.png", Texture.class));
        //
        //        resources.add(new AssetDef<>("ui_btn_sound_down",
        //                        "menu/buttons/ui_btn_sound_down.png", Texture.class));
        //        resources.add(new AssetDef<>("ui_btn_sound_up",
        //                        "menu/buttons/ui_btn_sound_up.png", Texture.class));
        //
        //        resources.add(new AssetDef<>("ui_btn_support_down",
        //                        "menu/buttons/ui_btn_support_down.png", Texture.class));
        //        resources.add(new AssetDef<>("ui_btn_support_up",
        //                        "menu/buttons/ui_btn_support_up.png", Texture.class));
        //
        //        resources.add(new AssetDef<>("ui_btn_settings_down",
        //                        "menu/buttons/ui_btn_settings_down.png", Texture.class));
        //        resources.add(new AssetDef<>("ui_btn_settings_up",
        //                        "menu/buttons/ui_btn_settings_up.png", Texture.class));

        resources.add(new AssetDef<>("menu_map_tz",
                        "menu/map/menu_map_tz.jpg", Texture.class));
        resources.add(new AssetDef<>("menu_map_znz",
                        "menu/map/menu_map_znz.jpg", Texture.class));

        resources.add(new AssetDef<>("menu_settings",
                        "menu/settings/menu_settings.jpg", Texture.class));

        resources.add(new AssetDef<>("menu_ency",
                        "menu/ency/menu_ency.jpg", Texture.class));
        resources.add(new AssetDef<>("menu_ency_plants",
                        "menu/ency/menu_ency_plants.jpg", Texture.class));

        resources.add(new AssetDef<>("menu_projects",
                        "menu/projects/menu_projects.jpg", Texture.class));
        resources.add(new AssetDef<>("menu_projects_popup",
                        "menu/projects/menu_projects_popup.jpg",
                        Texture.class));

        resources.add(new AssetDef<>("menu_shop",
                        "menu/shop/menu_shop.jpg", Texture.class));

        resources.add(new AssetDef<>("menu_shop_prime",
                        "menu/shop/menu_shop_prime.jpg", Texture.class));

        nextMenu = "ui_btn_start";
    }

    @Override
    protected void init() {
        startMenu = new StartMenu(game.menuSkin);
        startMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_start"));

        mapTZMenu = new MapTZMenu(game.menuSkin);
        mapTZMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_map_tz"));

        mapZNZMenu = new MapZNZMenu(game.menuSkin);
        mapZNZMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_map_znz"));

        projectsMenu = new ProjectsMenu(game.menuSkin);
        projectsMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_projects"));

        projectsPopup = new ProjectsPopupMenu(game.menuSkin);
        projectsPopup.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_projects_popup"));

        settingsMenu = new SettingsMenu(game.menuSkin);
        settingsMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_settings"));

        encyMenu = new EncyMenu(game.menuSkin);
        encyMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_ency"));

        encyPlantsMenu = new EncyPlantsMenu(game.menuSkin);
        encyPlantsMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_ency_plants"));

        shopMenu = new ShopMenu(game.menuSkin);
        shopMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_shop"));

        shopPrimeMenu = new ShopPrimeMenu(game.menuSkin);
        shopPrimeMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_shop_prime"));

        soundtrackMenu = new SoundtrackMenu(game.menuSkin);
        soundtrackMenu.setBackground(game.assetMngr
                        .createSpriteDrawable("menu_settings"));

        showMenu(nextMenu);
    }

    @Override
    public void preAct(float delta) {
    }

    @Override
    public void postAct(float delta) {
    }

    @Override
    public void preDraw() {
    }

    @Override
    public void postDraw() {
    }

    public void setEntryMenu(String menuName) {
        nextMenu = menuName;
    }

    public void showMenu(String menuName) {
        showMenu(getMenu(menuName));
    }

    private void showMenu(UiTable menu) {
        clear();

        currentMenu = menu;
        currentMenu.prepare();

        addActor(currentMenu);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
                    int button) {
        mouseDown = true;
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer,
                    int button) {
        if (!mouseDown) return true;

        Actor a;
        Vector2 v;
        screenToStageCoordinates(v = new Vector2(screenX, screenY));

        a = hit(v.x, v.y, true);
        if (a == null) {
            mouseDown = false;
            return false;
        }

        if (a.getName().equals("ui_btn_back")) {
            if (currentMenu.getName().equals("menu_ency")
                            || currentMenu.getName().contains("shop")) {
                showMenu(mapZNZMenu);
                return true;
            }
            if (currentMenu.getName().equals("menu_ency_plants")) {
                showMenu(encyMenu);
                return true;
            }
            if (currentMenu.getName().equals("menu_projects_popup")) {
                showMenu(projectsMenu);
            } else {
                showMenu(startMenu);
            }

            mouseDown = false;
            return true;
        }

        if (a.getName().equals("ui_btn_start")) {
            game.getScreen().loadIntro();

            return super.touchUp(screenX, screenY, pointer, button);
        }
        
        if (a.getName().equals("ui_btn_music")) {
        	game.settings.putBoolean("musicOn", !game.settings.getBoolean("musicOn"));
        }

        if (a.getName().contains("btn"))
            showMenu(getMenu(a.getName()));

        if (a.getName().equals("ui_btn_exit")) {
            Gdx.app.exit();
            mouseDown = false;
            return true;
        }
        mouseDown = false;
        return true;
    }

    private UiTable getMenu(String name) {
        if (name == null || name.equals("")) return null;

        if (name.equals("ui_btn_start")) return startMenu;

        if (name.equals("ui_btn_map_tz")) return mapTZMenu;
        if (name.equals("ui_btn_map_znz") || name.equals("ui_btn_map"))
            return mapZNZMenu;

        if (name.equals("ui_btn_settings")) return settingsMenu;

        if (name.equals("ui_btn_ency")) return encyMenu;
        if (name.equals("ui_btn_ency_plants")) return encyPlantsMenu;

        if (name.equals("ui_btn_shop")) return shopMenu;
        if (name.equals("ui_btn_shop_prime")) return shopPrimeMenu;

        if (name.equals("ui_btn_soundtrack")) return soundtrackMenu;

        if (name.equals("ui_btn_projects")) return projectsMenu;
        if (name.equals("ui_btn_projects_popup")) return projectsPopup;

        return null;
    }
}
