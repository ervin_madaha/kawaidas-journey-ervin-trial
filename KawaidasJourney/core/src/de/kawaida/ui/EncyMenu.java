package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class EncyMenu extends UiTable {

    public EncyMenu(MenuSkin skin) {
        super(skin, "menu_ency");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);

        encyPlants.setPosition(730, 75);
        encyPlants.setSize(275, 480);
        addActor(encyPlants);
    }
}