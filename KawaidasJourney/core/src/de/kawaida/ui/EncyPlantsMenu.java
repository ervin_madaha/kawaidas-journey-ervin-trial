package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class EncyPlantsMenu extends UiTable {

    public EncyPlantsMenu(MenuSkin skin) {
        super(skin, "menu_ency_plants");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);
    }
}