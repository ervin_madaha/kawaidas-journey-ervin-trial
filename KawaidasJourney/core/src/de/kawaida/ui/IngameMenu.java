package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import de.kawaida.ui.skins.MenuSkin;
import de.kawaida.ui.widgets.KJLabel;

public class IngameMenu extends UiTable {

    private Table menu;

    private KJLabel title;

    private StoryDialog dialog;

    public IngameMenu(MenuSkin skin) {
        super(skin, "menu_pause");

        menu = new Table(skin);
        menu.setName("ui_dialog");

        title = new KJLabel("Dialog", skin);

        dialog = new StoryDialog(skin);

        //        add(title);
        //        row();
    }

    public void preparePause() {
        clear();
        //        map.setPosition(530, 125);
        //        map.setSize(200, 150);
        //        addActor(map);
        //
        //        projects.setPosition(1000, 520);
        //        projects.setSize(320, 200);
        //        addActor(projects);

        menu.setName("ui_dialog_pause");
        menu.clear();

        title.setText("Game paused");
        menu.add(title);
        menu.row();

        menu.add(skin.get("ui_btn_map", TextButton.class)).width(150)
                        .height(100);
        menu.add(skin.get("ui_btn_restart", TextButton.class)).width(150)
                        .height(100);
        menu.add(skin.get("ui_btn_resume", TextButton.class)).width(150)
                        .height(100);
        menu.add(skin.get("ui_btn_exit", TextButton.class)).width(150)
                        .height(100);

        add(menu);
    }

    public void prepareLose() {
        //        clear();
        //        menu.setName("ui_dialog_lose");
        //        menu.clear();
        //
        //        title.setText("Save Me !!!");
        //        menu.add(title);
        //        menu.row();
        //
        //        //        dialog.getButtonTable().add(skin.get("ui_btn_map", TextButton.class)).width(100).height(50);
        //        menu.add(skin.get("ui_btn_restart", TextButton.class)).width(150).height(100);
        //        menu.add(skin.get("ui_btn_exit", TextButton.class)).width(150).height(100);
        //
        //        add(menu);
    }

    public void prepareWin() {
        //        clear();
        //        menu.setName("ui_dialog_win");
        //        menu.clear();
        //
        //        title.setText("You Win !!!");
        //        menu.add(title);
        //        menu.row();
        //
        //        //        dialog.getButtonTable().add(skin.get("ui_btn_map", TextButton.class)).width(100).height(50);
        //        menu.add(skin.get("ui_btn_restart", TextButton.class)).width(150).height(100);
        //        menu.add(skin.get("ui_btn_exit", TextButton.class)).width(150).height(100);
        //
        //        add(menu);
    }

    //    public void prepareDialog(VJXEntity leftSpeaker, VJXEntity rightSpeaker) {
    //        clear();
    //
    //        dialog.setSpeakers(leftSpeaker, rightSpeaker);
    //        add(dialog);
    //    }

    public StoryDialog getStoryDialog() {
        return dialog;
    }

    @Override
    public void prepare() {
        next.setPosition(850, 40);
        next.setSize(225, 90);
        addActor(next);
    }
}
