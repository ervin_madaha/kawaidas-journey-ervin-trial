package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class LoseMenu extends UiTable {

    public LoseMenu(Skin skin) {
        super(skin, "menu_lose");
    }

    @Override
    public void prepare() {
        mapZNZ.setPosition(250, 75);
        mapZNZ.setSize(320, 90);
        addActor(mapZNZ);

        restart.setPosition(650, 75);
        restart.setSize(320, 90);
        addActor(restart);

        shop.setPosition(600, 620);
        shop.setSize(110, 70);
        addActor(shop);
    }

}
