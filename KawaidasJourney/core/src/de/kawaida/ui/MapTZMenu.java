package de.kawaida.ui;

import de.kawaida.ui.skins.KJSkin;

public class MapTZMenu extends UiTable {

    //    private Label selectSkinLabel, selectLevelLabel;
    //    private SelectBox<String> skinSelect, levelSelect;

    public MapTZMenu(KJSkin skin) {
        super(skin, "menu_map_tz");

        //        selectSkinLabel = new Label("Select graphic set:",
        //                        skin.get("style_label_solid", LabelStyle.class));
        //        selectSkinLabel.setName("ui_label_lose");
        //
        //        selectLevelLabel = new Label("Select level:",
        //                        skin.get("style_label_solid", LabelStyle.class));
        //        selectLevelLabel.setName("ui_label_lose");
        //
        //        skinSelect = new SelectBox<>(skin.getDfltSkin());
        //        skinSelect.setName("ui_select_skin");
        //        skinSelect.setItems(new String[] { "Kawaida" });
        //
        //        levelSelect = new SelectBox<>(skin.getDfltSkin());
        //        levelSelect.setName("ui_select_level");
        //        levelSelect.setItems(new String[] { "devel#01", "workshop#01",
        //                                            "workshop#02", "workshop#03",
        //                                            "workshop#04", "workshop#05"});
        //        levelSelect.getList().setName("ui_list_levels");

        //        setWidth(200);
    }

    @Override
    public void prepare() {
        mapZNZ.setPosition(0, 0);
        mapZNZ.setSize(1280, 720);
        addActor(mapZNZ);
    }

    //    public String getLevelName() {
    //        return levelSelect.getSelected();
    //    }
}