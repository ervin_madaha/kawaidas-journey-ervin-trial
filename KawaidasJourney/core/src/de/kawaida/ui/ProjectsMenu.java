package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ProjectsMenu extends UiTable {

    public ProjectsMenu(Skin skin) {
        super(skin, "menu_projects");
    }

    @Override
    public void prepare() {
        back.setPosition(40, 610);
        back.setSize(90, 90);
        addActor(back);

        projectsPopup.setPosition(60, 75);
        projectsPopup.setSize(275, 480);
        addActor(projectsPopup);
    }

}