package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ProjectsPopupMenu extends UiTable {

    public ProjectsPopupMenu(Skin skin) {
        super(skin, "menu_projects_popup");
    }

    @Override
    public void prepare() {
        back.setPosition(490, 50);
        back.setSize(320, 90);
        addActor(back);
    }

}