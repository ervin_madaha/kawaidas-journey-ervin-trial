package de.kawaida.ui;

import de.kawaida.ui.skins.MenuSkin;

public class StartMenu extends UiTable {

    public StartMenu(MenuSkin skin) {
        super(skin, "menu_start");
    }

    @Override
    public void prepare() {
        mapTZ.setPosition(530, 125);
        mapTZ.setSize(200, 150);
        addActor(mapTZ);

        projects.setPosition(1000, 520);
        projects.setSize(320, 200);
        addActor(projects);

        settings.setPosition(40, 610);
        settings.setSize(90, 90);
        addActor(settings);
    }
}