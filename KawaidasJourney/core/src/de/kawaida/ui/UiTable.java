package de.kawaida.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class UiTable extends Table {

    //    public static final int M_START = 0;
    //    public static final int M_MAP = 1;
    //    public static final int M_SETTINGS = 2;
    //    public static final int M_ENCY = 3;
    //    public static final int M_SHOP = 4;

    //    protected int id;
    protected Skin skin;

    protected Image bgImage;

    protected Actor mapTZ = new Actor();
    protected Actor mapZNZ = new Actor();
    protected Actor back = new Actor();
    protected Actor projects = new Actor();
    protected Actor projectsPopup = new Actor();
    protected Actor ency = new Actor();
    protected Actor encyPlants = new Actor();
    protected Actor start = new Actor();
    protected Actor restart = new Actor();
    protected Actor resume = new Actor();
    protected Actor close = new Actor();
    protected Actor shop = new Actor();
    protected Actor shopPrime = new Actor();
    protected Actor settings = new Actor();
    protected Actor next = new Actor();
    protected Actor music = new Actor();

    public UiTable(Skin skin, String name) {
        super(skin);
        this.skin = skin;

        setName(name);

        if (skin.has(name + "_bg", Texture.class))
            setBackground(name + "_bg");

        //        setTouchable(Touchable.enabled);

        bgImage = new Image();
        bgImage.setName("ui_table_bg");
        add(bgImage);

        mapTZ.setName("ui_btn_map_tz");
        mapZNZ.setName("ui_btn_map_znz");
        back.setName("ui_btn_back");
        projects.setName("ui_btn_projects");
        projectsPopup.setName("ui_btn_projects_popup");
        ency.setName("ui_btn_ency");
        encyPlants.setName("ui_btn_ency_plants");
        start.setName("ui_btn_start");
        restart.setName("ui_btn_restart");
        resume.setName("ui_btn_resume");
        close.setName("ui_btn_close");
        shop.setName("ui_btn_shop");
        shopPrime.setName("ui_btn_shop_prime");
        settings.setName("ui_btn_settings");
        next.setName("ui_btn_next");
        music.setName("ui_btn_music");

        setFillParent(true);
        setDebug(true);
    }

    public abstract void prepare();
}
