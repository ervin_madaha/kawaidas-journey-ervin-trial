package de.kawaida.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.SnapshotArray;

import de.kawaida.kjgame.entities.items.Banana;
import de.kawaida.ui.skins.InterfaceSkin;
import de.venjinx.core.libgdx.Player;
import de.venjinx.core.libgdx.assets.VJXIcon;

public class UserInterface extends UiTable {
    private Touchpad tp;

    private Button pauseButton;

    private Table statusBar;
    private VJXIcon sunsIcon, cocosIcon, totemsIcon;

    public HorizontalGroup bananas;

    public UserInterface(InterfaceSkin skin, String name) {
        super(skin, name);

        tp = skin.get("ui_joystick", Touchpad.class);
        //        tp.setResetOnTouchUp(true);

        statusBar = new Table();
        statusBar.setName("ui_status_bar");
        statusBar.defaults().pad(25);

        sunsIcon = skin.get("ui_icon_sun", VJXIcon.class);
        totemsIcon = skin.get("ui_icon_totem", VJXIcon.class);

        statusBar.add(sunsIcon);
        statusBar.add(totemsIcon);

        bananas = new HorizontalGroup();
        bananas.setName("ui_bananas");
        Image banana;
        for (int i = 0; i < 4; i++) {
            banana = new Image(skin.getDrawable("item_banana"));
            banana.setName("ui_icon_banana");
            bananas.addActor(banana);
        }
        statusBar.add(bananas);

        pauseButton = skin.get("ui_btn_pause", Button.class);

        cocosIcon = skin.get("ui_btn_cocos", VJXIcon.class);

        clear();

        add(statusBar).expand().top().left();
        add(pauseButton).expand().top().right().pad(25);
        row();
        add();
        add(cocosIcon).expand().bottom().right().width(96).height(96).pad(25);
    }

    public void update(Player player) {
        Banana b = player.getAvatar().getBanana();
        int amount = b.getAmount();

        b.getAvatarIcon();

        SnapshotArray<Actor> tmpArray = bananas.getChildren();
        for (int i = 0; i < 4; i++) {
            if (i < amount) tmpArray.items[i].setColor(Color.WHITE);
            else tmpArray.items[i].setColor(Color.GRAY);
        }

        sunsIcon.setAmount(player.getSun().getAmount());
        cocosIcon.setAmount(player.getAvatar().getWeapon().getAmount());
        totemsIcon.setAmount(player.getAvatar().getTotem().getAmount());
    }

    @Override
    public void prepare() {

    }

    public Touchpad getJoystick() {
        return tp;
    }

    public void setJoysticPos(float x, float y) {
        tp.setPosition(x - tp.getWidth() / 2, y - tp.getHeight() / 2);
    }

    public boolean paused() {
        return pauseButton.isChecked();
    }
}
