package de.kawaida.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class WinMenu extends UiTable {

    public WinMenu(Skin skin) {
        super(skin, "menu_win");
    }

    @Override
    public void prepare() {
        mapZNZ.setPosition(900, 80);
        mapZNZ.setSize(320, 90);
        addActor(mapZNZ);

        shop.setPosition(1000, 600);
        shop.setSize(250, 90);
        addActor(shop);
    }

}
