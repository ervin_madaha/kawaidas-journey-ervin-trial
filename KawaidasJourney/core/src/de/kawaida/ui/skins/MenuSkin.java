package de.kawaida.ui.skins;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import de.venjinx.core.libgdx.VJXAssetManager;
import de.venjinx.core.libgdx.VJXFont;

public class MenuSkin extends KJSkin {

    private Button fbBtn;
    private Button start;
    private Button sound;
    private Button projects;
    private Button settings;

    private TextButton nextBtn;
    private TextButton resumeBtn;
    private TextButton restartBtn;
    private TextButton backBtn;
    private TextButton mapBtn;
    private TextButton settingsBtn;
    private TextButton encyBtn;
    private TextButton shopBtn;
    private TextButton startBtn;
    private TextButton exitBtn;

    public MenuSkin(VJXFont font, VJXAssetManager am) {
        super(null, font);

        fbBtn = new Button(am.createSpriteDrawable("ui_btn_fb_up"),
                        am.createSpriteDrawable("ui_btn_fb_down"));
        fbBtn.setName("ui_btn_fb");
        add("ui_btn_fb", fbBtn);

        start = new Button(am.createSpriteDrawable("ui_btn_fb_up"),
                        am.createSpriteDrawable("ui_btn_fb_down"));
        start.setName("ui_btn_start");
        add("ui_btn_start", start);

        sound = new Button(am.createSpriteDrawable("ui_btn_fb_up"),
                        am.createSpriteDrawable("ui_btn_fb_down"));
        sound.setName("ui_btn_sound");
        add("ui_btn_sound", sound);

        projects = new Button(am.createSpriteDrawable("ui_btn_fb_up"),
                        am.createSpriteDrawable("ui_btn_fb_down"));
        projects.setName("ui_btn_projects");
        add("ui_btn_projects", projects);

        settings = new Button(am.createSpriteDrawable("ui_btn_fb_up"),
                        am.createSpriteDrawable("ui_btn_fb_down"));
        settings.setName("ui_btn_settings");
        add("ui_btn_settings", settings);

        //        start = new Button(am.createSpriteDrawable("ui_btn_start_up"),
        //                        am.createSpriteDrawable("ui_btn_start_down"));
        //        start.setName("ui_btn_start");
        //        add("ui_btn_start", start);
        //
        //        sound = new Button(am.createSpriteDrawable("ui_btn_sound_up"),
        //                        am.createSpriteDrawable("ui_btn_sound_down"));
        //        sound.setName("ui_btn_sound");
        //        add("ui_btn_sound", sound);
        //
        //        support = new Button(am.createSpriteDrawable("ui_btn_support_up"),
        //                        am.createSpriteDrawable("ui_btn_support_down"));
        //        support.setName("ui_btn_support");
        //        add("ui_btn_support", support);
        //
        //        settings = new Button(am.createSpriteDrawable("ui_btn_settings_up"),
        //                        am.createSpriteDrawable("ui_btn_settings_down"));
        //        settings.setName("ui_btn_settings");
        //        add("ui_btn_settings", settings);

        nextBtn = new TextButton("Next", defaultWidgets);
        nextBtn.getLabel().setTouchable(Touchable.disabled);
        nextBtn.setName("ui_btn_next");
        add("ui_btn_next", nextBtn);

        restartBtn = new TextButton("Restart", defaultWidgets);
        restartBtn.getLabel().setTouchable(Touchable.disabled);
        restartBtn.setName("ui_btn_restart");
        add("ui_btn_restart", restartBtn);

        resumeBtn = new TextButton("Resume", defaultWidgets);
        resumeBtn.getLabel().setTouchable(Touchable.disabled);
        resumeBtn.setName("ui_btn_resume");
        add("ui_btn_resume", resumeBtn);

        backBtn = new TextButton("Back", defaultWidgets);
        backBtn.getLabel().setTouchable(Touchable.disabled);
        backBtn.setName("ui_btn_back");
        add("ui_btn_back", backBtn);

        mapBtn = new TextButton("Map", defaultWidgets);
        mapBtn.getLabel().setTouchable(Touchable.disabled);
        mapBtn.setName("ui_btn_map");
        add("ui_btn_map", mapBtn);

        settingsBtn = new TextButton("Settings", defaultWidgets);
        settingsBtn.getLabel().setTouchable(Touchable.disabled);
        settingsBtn.setName("ui_btn_settings");
        add("ui_btn_settings", settingsBtn);

        encyBtn = new TextButton("Encyclopedia", defaultWidgets);
        encyBtn.getLabel().setTouchable(Touchable.disabled);
        encyBtn.setName("ui_btn_ency");
        add("ui_btn_ency", encyBtn);

        shopBtn = new TextButton("Shop", defaultWidgets);
        shopBtn.getLabel().setTouchable(Touchable.disabled);
        shopBtn.setName("ui_btn_shop");
        add("ui_btn_shop", shopBtn);

        startBtn = new TextButton("Start", defaultWidgets);
        startBtn.getLabel().setTouchable(Touchable.disabled);
        startBtn.setName("ui_btn_start");
        add("ui_btn_start", startBtn);

        exitBtn = new TextButton("Exit", defaultWidgets);
        exitBtn.getLabel().setTouchable(Touchable.disabled);
        exitBtn.setName("ui_btn_exit");
        add("ui_btn_exit", exitBtn);
    }

}