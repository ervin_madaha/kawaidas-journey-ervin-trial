package de.kawaida.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class KJLabel extends Stack {
    private Label solid;
    private Label outline;
    private Label shadow;

    public KJLabel(String text, Skin skin) {
        solid = new Label(text, skin, "style_label_solid");
        solid.setAlignment(Align.topRight, Align.right);
        solid.setTouchable(Touchable.disabled);
        add(solid);

        shadow = new Label(text, skin, "style_label_shadow");
        shadow.setAlignment(Align.topRight, Align.right);
        shadow.setTouchable(Touchable.disabled);
        add(shadow);

        outline = new Label(text, skin, "style_label_outline");
        outline.setAlignment(Align.topRight, Align.right);
        outline.setTouchable(Touchable.disabled);
        add(outline);

        setTouchable(Touchable.disabled);
    }

    public void setText(String text) {
        solid.setText(text);
        outline.setText(text);
        shadow.setText(text);
    }
}
