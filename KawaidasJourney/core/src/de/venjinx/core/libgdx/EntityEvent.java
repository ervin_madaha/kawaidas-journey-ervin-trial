package de.venjinx.core.libgdx;

import com.badlogic.gdx.scenes.scene2d.Event;

import de.venjinx.core.libgdx.entity.VJXEntity;

public class EntityEvent extends Event {

    public String name = "";

    public EntityEvent(VJXEntity src, String name) {
        this(src, src, name);
        this.name = name;
    }

    public EntityEvent(VJXEntity src, VJXEntity target, String name) {
        setListenerActor(src);
        setTarget(target);
        setBubbles(false);
        setStage(src.getStage());
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}