package de.venjinx.core.libgdx;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;

import de.kawaida.kjgame.animation.Animation;
import de.kawaida.kjgame.animation.Animation.AnimationEvent;
import de.kawaida.kjgame.entities.enemies.Bird;
import de.kawaida.kjgame.entities.level.KJLevel;
import de.kawaida.kjgame.screens.GameScreen;
import de.kawaida.kjgame.stages.IngameHUD;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.entity.Character;

public class EntityListener implements EventListener {

    private KJLevel lvl;
    private IngameHUD ui;

    public EntityListener(GameScreen screen) {
        lvl = screen.getLevel();
        ui = screen.getHUD();
    }

    @Override
    public boolean handle(Event event) {
        Actor eActor = event.getListenerActor();
        Actor target = event.getTarget();

        if (event instanceof AnimationEvent) {
            Character c = (Character) eActor;
            String cName = c.getName();
            AnimationEvent ae = (AnimationEvent) event;
            Animation anim = ae.getAnimation();


            if (cName.contains("palm")) {
                if (ae.getName().contains("finished")) ui.showProject();
                //                System.out.println("------------Handle change event----------");
                //                System.out.println("Event      : " + event);
                //                System.out.println("EventActor : " + eActor.getName());
                //                System.out.println("EventTarget: " + target.getName());
                //                System.out.println("-----------------------------------------");
                //                System.out.println();
            }

            if (cName.contains("kawaida")) {
                //                System.out.println("------------Handle change event----------");
                //                System.out.println("Event      : " + event);
                //                System.out.println("EventActor : " + eActor.getName());
                //                System.out.println("EventTarget: " + target.getName());
                //                System.out.println("-----------------------------------------");
                //                System.out.println();

                if (ae.getName().contains("finished")) {
                    if (anim.getName().contains("hit")
                                    || anim.getName().contains("throw")) {

                        if (anim.getName().contains("kawaida"))
                            System.out.println(c.getStatus());
                        //                    if (!c.isDead()) anim.reset();
                        c.setStatus(VJXStatus.IDLE, true);
                    }
                    if (!c.isDead()) anim.reset();
                }

                if (anim.getName().contains("die")) {
                    //                    c.setAlive(false);
                    //                    if (!c.isDead()) anim.reset();
                    //                    c.setStatus(VJXStatus.IDLE, true);
                }
            }

            if (cName.contains("bird")) {
                if (ae.getName().contains("fly"))
                    ((Bird) c).flap();
            }
        }
        return false;
    }

}
