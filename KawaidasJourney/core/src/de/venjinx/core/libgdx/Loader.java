package de.venjinx.core.libgdx;

import java.util.ArrayDeque;

import com.badlogic.gdx.graphics.Texture;

import de.kawaida.kjgame.stages.Menu;
import de.kawaida.ui.UiTable;
import de.kawaida.ui.widgets.KJLabel;
import de.venjinx.core.libgdx.assets.AssetDef;

public class Loader extends UiTable {
    private VJXGame game;

    private VJXStage currentStage;

    private ArrayDeque<VJXStage> stagesToLoad;
    private ArrayDeque<VJXStage> stagesToUnload;
    private ArrayDeque<VJXStage> loadedStages;

    private String loadStr = "Please wait...loading - ";
    private KJLabel progressLabel;

    private boolean processing = false;
    private boolean loading = false;
    private boolean unloading = false;

    public Loader(VJXGame game) {
        super(game.menuSkin, "loader");

        this.game = game;

        game.assetMngr.loadResource(new AssetDef<>("loading",
                        "sprites/loading.jpg", Texture.class));

        game.assetMngr.loadResource(new AssetDef<>("lvl_loading",
                        "menu/map/menu_map_loading.jpg", Texture.class));
        game.assetMngr.finishLoading();

        stagesToLoad = new ArrayDeque<>();
        stagesToUnload = new ArrayDeque<>();
        loadedStages = new ArrayDeque<>();

        setBackground(game.assetMngr.createSpriteDrawable("loading"));

        progressLabel = new KJLabel(loadStr, game.menuSkin);
        add(progressLabel);

        //        layout();
    }

    @Override
    public void prepare() {

    }

    public boolean processNext() {
        if (processing) return false;

        currentStage = null;

        loading = false;
        unloading = false;

        if (!stagesToUnload.isEmpty()) {
            currentStage = stagesToUnload.pollFirst();
            currentStage.dispose();

            for (AssetDef<? extends Object> ad : currentStage.getAssetList()) {
                if (game.assetMngr.isLoaded(ad.fileName))
                    game.assetMngr.unloadResource(ad);
            }
            unloading = true;
            processing = true;

            if (currentStage instanceof Menu)
                setBackground(game.assetMngr.createSpriteDrawable("loading"));
            else setBackground(game.assetMngr.createSpriteDrawable("lvl_loading"));
            return true;
        }

        if (!stagesToLoad.isEmpty()) {
            currentStage = stagesToLoad.pollFirst();

            for (AssetDef<? extends Object> ad : currentStage.getAssetList()) {
                if (!game.assetMngr.isLoaded(ad.fileName)) {
                    game.assetMngr.loadResource(ad);
                }
            }
            loading = true;
            processing = true;

            if (currentStage instanceof Menu) setBackground(
                            game.assetMngr.createSpriteDrawable("loading"));
            else setBackground(game.assetMngr.createSpriteDrawable("lvl_loading"));
            return true;
        }

        return false;
    }

    public float getProgress() {
        int progress = (int) (game.assetMngr.getProgress() * 10000);

        return progress / 100f;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        //        assert !(!loading && unLoading);

        if (loading || unloading) {
            game.assetMngr.update();
            progressLabel.setText(loadStr + getProgress() + "%");
        }

        if (game.assetMngr.getProgress() == 1f) {
            if (loading) {
                //                System.out.println("Prepare: " + currentStage);
                currentStage.prepare();
                loadedStages.add(currentStage);
            }

            processing = false;

            progressLabel.setText("Finished loading. Click to start.");

            //            System.out.println("Finished: " + currentStage);
            //            System.out.println("----------------------------------");
            //            System.out.println();

            processNext();
        }
    }

    public void unload(VJXStage stage) {
        //        System.out.println("Add to unload: " + stage);
        stagesToUnload.add(stage);
    }

    public void load(VJXStage stage) {
        //        System.out.println("Add to load: " + stage);
        stagesToLoad.add(stage);
    }

    public boolean isProcessing() {
        return processing;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isUnloading() {
        return unloading;
    }

    public ArrayDeque<VJXStage> getLoadedStages() {
        return loadedStages;
    }

    public boolean hasNewData() {
        return !loadedStages.isEmpty();
    }
}