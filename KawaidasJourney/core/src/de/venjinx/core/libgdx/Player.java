package de.venjinx.core.libgdx;

import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.kjgame.entities.items.Sun;

public class Player {

    private String name;

    private Avatar avatar;

    private Sun sun;

    public Player(String name) {
        setName(name);

        sun = new Sun("item_sun");
    }

    public void setAvatar(Avatar a) {
        avatar = a;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Sun getSun() {
        return sun;
    }

}