package de.venjinx.core.libgdx;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class TmxLoader extends TmxMapLoader {

    public TmxLoader() {
        super(new InternalFileHandleResolver());
    }

    public TmxLoader(FileHandleResolver resolver) {
        super(resolver);
    }
}
