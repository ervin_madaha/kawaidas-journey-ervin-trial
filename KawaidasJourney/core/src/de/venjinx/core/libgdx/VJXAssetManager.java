package de.venjinx.core.libgdx;

import java.util.Hashtable;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.resolvers.AbsoluteFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.venjinx.core.libgdx.assets.AssetDef;

public class VJXAssetManager extends AssetManager {

    public TmxLoader intMapLoader;
    public TmxLoader absMapLoader;
    public SoundLoader soundLoader;
    public MusicLoader musicLoader;

    private Hashtable<String, AssetDef<? extends Object>> res;

    public VJXAssetManager() {
        res = new Hashtable<>();

        intMapLoader = new TmxLoader(new InternalFileHandleResolver());
        absMapLoader = new TmxLoader(new AbsoluteFileHandleResolver());
        soundLoader = new SoundLoader(new InternalFileHandleResolver());
        musicLoader = new MusicLoader(new InternalFileHandleResolver());
    }

    public synchronized void loadResource(AssetDef<? extends Object> descr) {
        load(descr);
        res.put(descr.name, descr);
    }

    public synchronized void unloadResource(AssetDef<? extends Object> descr) {
        unload(descr.fileName);
        res.remove(descr);
    }

    public synchronized TiledMap loadMap(String path, String name, boolean internal) {

        TiledMap m;
        if (internal) m = intMapLoader.load(path);
        else {
            m = absMapLoader.load(path);
        }

        addAsset(name, TiledMap.class, m);
        return m;
    }

    public synchronized Texture getTexture(String name) {
        return get(res.get(name).fileName, Texture.class);
    }

    public synchronized Drawable drawableFromSheet(String sheet, String name) {
        TextureRegionDrawable d = new TextureRegionDrawable(getRegion(sheet, name));
        return d;
    }

    public synchronized Sprite createSprite(String name) {
        return new Sprite(getTexture(name));
    }

    public synchronized SpriteDrawable createSpriteDrawable(String name) {
        return new SpriteDrawable(createSprite(name));
    }

    public synchronized Sound getSound(String name) {
        return get(res.get(name).fileName, Sound.class);
    }

    public synchronized Music getMusic(String name) {
        return get(res.get(name).fileName, Music.class);
    }

    public synchronized AtlasRegion getRegion(String sheet, String name) {
        TextureAtlas ta = get(res.get(sheet).fileName, TextureAtlas.class);

        return ta.findRegion(name);
    }

    public synchronized TextureAtlas getSpriteSheet(String name) {
        return get(res.get(name).fileName, TextureAtlas.class);
    }

    public synchronized TiledMap getMap(String name) {
        return get(name, TiledMap.class);
    }

    public synchronized void disposeMap(String name) {
        if (isLoaded(name)) {
            unload(name);
        }
    }
}