package de.venjinx.core.libgdx;

import de.venjinx.core.libgdx.entity.VJXEntity;

public class VJXData {

    private String name;
    private VJXEntity entity;

    public VJXData(String name, VJXEntity entity) {
        this.name = name;
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public VJXEntity getEntity() {
        return entity;
    }
}