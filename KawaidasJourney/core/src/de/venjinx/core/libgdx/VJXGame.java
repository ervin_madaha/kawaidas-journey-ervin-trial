package de.venjinx.core.libgdx;

import java.util.ArrayDeque;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.kawaida.kjgame.controllers.CollisionControl;
import de.kawaida.kjgame.screens.GameScreen;
import de.kawaida.ui.skins.MenuSkin;
import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.entity.EntityFactory;

public class VJXGame extends Game {

    private static long currentID = 0;
    private static ArrayDeque<Long> freeIDs = new ArrayDeque<Long>();

    public Vector2 res = new Vector2();
    public Vector2 targetRes = new Vector2();

    protected float timePassed = 0;
    protected long timeUpdate = 0;
    protected long timeRender = 0;

    private float step = 1f / 60f;

    private OrthographicCamera cam;
    private OrthographicCamera b2dCam;
    private OrthographicCamera interfaceCam;

    private FitViewport view;
    private FitViewport interfaceView;
    private FreeTypeFontGenerator generator;

    public InputMultiplexer inputControl;
    public CollisionControl cCtrl;

    public SpriteBatch batch;
    public VJXFont font;
    public VJXAssetManager assetMngr;
    public EntityFactory factory;
    //    public AudioController audioController;

    public MenuSkin menuSkin;

    private Player player;
    private GameScreen gameScreen;

    public DebugStage dbgStage;

    private boolean paused = false;

    private String[] args;
    
    public Preferences settings;

    public VJXGame() {
    }

    public VJXGame(String[] args) {
        this.args = args;

    }

    @Override
    public void create () {
        //        audioController = new AudioController();
        assetMngr = new VJXAssetManager();
        factory = new EntityFactory(this);

        res.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //        targetRes.set(960, 540);
        targetRes.set(1280, 720);

        cam = new OrthographicCamera();

        b2dCam = new OrthographicCamera();

        interfaceCam = new OrthographicCamera();

        view = new FitViewport(targetRes.x, targetRes.y, cam);
        //        view.setScreenSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        interfaceView = new FitViewport(targetRes.x, targetRes.y, interfaceCam);
        //        interfaceView.setScreenSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        //        avatar = new Avatar(this);
        player = new Player("player");

        Gdx.input.setInputProcessor(inputControl = new InputMultiplexer());
        cCtrl = new CollisionControl();

        batch = new SpriteBatch();
        batch.setProjectionMatrix(cam.combined);

        loadFont();

        assetMngr.loadResource(new AssetDef<>("ui_btn_fb_up",
                        "menu/buttons/ui_btn_fb_up.png", Texture.class));
        assetMngr.loadResource(new AssetDef<>("ui_btn_fb_down",
                        "menu/buttons/ui_btn_fb_down.png", Texture.class));

        //        assetMngr.loadResource(new AssetDef<>("ui_btn_start_up",
        //                        "menu/buttons/ui_btn_start_up.png", Texture.class));
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_start_down",
        //                        "menu/buttons/ui_btn_start_down.png", Texture.class));
        //
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_sound_up",
        //                        "menu/buttons/ui_btn_sound_up.png", Texture.class));
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_sound_down",
        //                        "menu/buttons/ui_btn_sound_down.png", Texture.class));
        //
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_support_up",
        //                        "menu/buttons/ui_btn_support_up.png", Texture.class));
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_support_down",
        //                        "menu/buttons/ui_btn_support_down.png", Texture.class));
        //
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_settings_up",
        //                        "menu/buttons/ui_btn_settings_up.png", Texture.class));
        //        assetMngr.loadResource(new AssetDef<>("ui_btn_settings_down",
        //                        "menu/buttons/ui_btn_settings_down.png", Texture.class));
        assetMngr.finishLoading();
        menuSkin = new MenuSkin(font, assetMngr);

        dbgStage = new DebugStage(this);

        gameScreen = new GameScreen(this);
        setScreen(gameScreen);

        if (args == null || args.length == 0)
            gameScreen.loadMenu("ui_btn_start");
        else gameScreen.loadLevel(args[0], false);

        inputControl.addProcessor(dbgStage);
        
        settings = Gdx.app.getPreferences("game_settings");
        settings.putBoolean("soundOn", true);
        settings.putBoolean("musicOn", true);

        //        Gdx.input.setCatchBackKey(true);
    }

    public long gameRender = 0;
    //    private float deltaScaled = 0;

    @Override
    public void render () {
        //        System.out.println("------------------begin main loop------------------");
        Gdx.gl20.glClearColor(0, 0, 0, 1);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!paused) {
            gameRender = System.currentTimeMillis();

            //            System.out.println("----------------------game update-----------------------");

            timePassed += Gdx.graphics.getDeltaTime();

            timeUpdate = System.currentTimeMillis();
            if (timePassed >= step) {
                //                    b2dWorld.step(step, 6, 2);
                getScreen().update(Gdx.graphics.getDeltaTime());
                timePassed -= step;
            }
            timeUpdate = System.currentTimeMillis() - timeUpdate;

            //                dbgStage.act(nextStep);
            timeRender = System.currentTimeMillis();

            //            System.out.println("--------------------game update end---------------------\n");

            //            System.out.println("----------------------game render-----------------------");

            //            System.out.println("    -----------------screen render------------------    ");
            //            dbgStage.draw();
            //            System.out.println("    ---------------screen render end----------------    ");

            //            System.out.println("--------------------game render end---------------------\n\n");

            //        if (Gdx.input.isKeyPressed(Keys.BACK)) {
            //            dispose();
            //            Gdx.app.exit();
            //        }
            //            System.out.println(player.getListeners().size);
        }
        getScreen().render(Gdx.graphics.getDeltaTime());
        //        System.out.println("-------------------end main loop-------------------\n");
    }

    @Override
    public GameScreen getScreen() {
        return (GameScreen) super.getScreen();
    }

    @Override
    public void dispose() {
        super.dispose();
        font.dispose();
        generator.dispose();
        batch.dispose();
        if (gameScreen != null) gameScreen.dispose();
        assetMngr.dispose();
        //        audioController.dispose();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void resize(int width, int height) {
        b2dCam.setToOrtho(false, view.getWorldWidth() / 100f,
                            view.getWorldHeight() / 100f);
        res.set(width, height);
        super.resize(width, height);
        view.update(width, height, true);
        interfaceView.update(width, height, true);
    }

    private void loadFont() {
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = (int) Math.ceil(25);
        parameter.minFilter = TextureFilter.Nearest;
        parameter.magFilter = TextureFilter.MipMapLinearNearest;

        BitmapFont s, out, sh;
        String fileName = "fonts/londrina/LondrinaSolid-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        s = generator.generateFont(parameter);

        parameter.size = (int) Math.ceil(25);
        fileName = "fonts/londrina/LondrinaOutline-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        out = generator.generateFont(parameter);

        parameter.size = (int) Math.ceil(25);
        fileName = "fonts/londrina/LondrinaShadow-Regular.otf";
        generator = new FreeTypeFontGenerator(Gdx.files.internal(fileName));
        generator.scaleForPixelHeight((int) Math.ceil(25));
        sh = generator.generateFont(parameter);

        font = new VJXFont(s, out, sh);
    }

    public Player getPlayer() {
        return player;
    }

    public OrthographicCamera getCam() {
        return cam;
    }

    public OrthographicCamera getB2DCam() {
        return b2dCam;
    }

    public OrthographicCamera getInterfaceCam() {
        return interfaceCam;
    }

    public FitViewport getView() {
        return view;
    }

    public FitViewport getInterfaceView() {
        return interfaceView;
    }

    public static long newID() {
        long id = -1;
        if (!freeIDs.isEmpty()) id = freeIDs.removeFirst();
        else id = currentID++;

        return id;
    }

    public static long freeID(long id) {
        freeIDs.add(id);

        return id;
    }
}