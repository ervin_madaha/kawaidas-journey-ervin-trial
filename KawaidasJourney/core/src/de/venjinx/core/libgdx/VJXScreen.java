package de.venjinx.core.libgdx;

import java.util.ArrayDeque;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.kawaida.kjgame.entities.actors.Avatar;

public abstract class VJXScreen implements Screen {

    private ArrayList<VJXStage> stages = new ArrayList<VJXStage>();

    protected VJXGame game;
    protected OrthographicCamera cam;
    protected OrthographicCamera b2dCam;
    protected Avatar player;

    protected boolean paused = false;

    private long frameNr = 0;

    private Stage loadStage;
    protected Loader loader;

    public VJXScreen(VJXGame game) {
        this.game = game;
        player = game.getPlayer().getAvatar();

        cam = game.getCam();
        b2dCam = game.getB2DCam();

        loader = new Loader(game);

        loadStage = new Stage(game.getView(), game.batch);
        loadStage.addActor(loader);
    }

    protected abstract void userUpdate(float deltaT);

    protected abstract void userRender(float deltaT);

    public abstract void restart();

    public void addStage(VJXStage stage) {
        if (stages.contains(stage)) return;

        if (!stage.isPrepared()) {
            loader.load(stage);
        } else {
            stages.add(stage);
            game.inputControl.addProcessor(stage);
        }
    }

    public boolean removeStage(VJXStage stage) {
        if (!stages.contains(stage)) return false;

        loader.unload(stage);

        game.inputControl.removeProcessor(stage);
        return stages.remove(stage);
    }

    private void fetchStages() {
        ArrayDeque<VJXStage> stages = loader.getLoadedStages();
        this.stages.addAll(stages);

        while (!stages.isEmpty())
            game.inputControl.addProcessor(stages.pollFirst());
    }

    public void update(float deltaT) {

        if (Gdx.input.isButtonPressed(0) && !loader.isLoading()) fetchStages();

        if (!loader.isProcessing()) {
            if (Gdx.input.isButtonPressed(0)) fetchStages();
            //            System.out.println("    -----------------screen update------------------    ");

            if (!paused) {
                frameNr++;

                for (VJXStage s : stages)
                    s.act(deltaT);

                userUpdate(deltaT);

                //            dbgStage.act(deltaT);

                //            pause();
                //            System.out.println("    ---------------screen update end----------------    \n");
            }
        } else {
            loadStage.act(deltaT);
        }
    }

    @Override
    public void render(float delta) {
        if (!loader.isLoading() && !stages.isEmpty()) {
            for (VJXStage s : stages) {
                s.draw();
            }

            game.dbgStage.draw();
            userRender(delta);
        } else {
            loadStage.draw();
        }
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void show() {
    }

    public boolean isPaused() {
        return paused;
    }

    @Override
    public void resize(int width, int height) {
    }

    public long getFrameNr() {
        return frameNr;
    }

    public void setDebug(boolean debug) {
        for (VJXStage s : stages)
            s.setDebugAll(debug);
    }
}