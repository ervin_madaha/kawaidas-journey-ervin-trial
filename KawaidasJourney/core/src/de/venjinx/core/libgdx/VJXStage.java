package de.venjinx.core.libgdx;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.SnapshotArray;

import de.venjinx.core.libgdx.assets.AssetDef;
import de.venjinx.core.libgdx.entity.VJXEntity;
import de.venjinx.core.libgdx.entity.VJXGroup;

public abstract class VJXStage extends Stage {

    private String name = "VJXStage";

    protected VJXGame game;

    protected Player player;

    protected HashMap<Long, VJXEntity> entities;

    protected ArrayList<AssetDef<? extends Object>> resources;

    private boolean prepared = false;

    public VJXStage(VJXGame game) {
        super(game.getView(), game.batch);
        this.game = game;
        player = game.getPlayer();
        entities = new HashMap<>();
        resources = new ArrayList<>();
    }

    @Override
    public void act(float delta) {
        preAct(delta);
        super.act(delta);
        postAct(delta);
    }

    @Override
    public void draw() {
        preDraw();
        super.draw();
        postDraw();
    }

    protected abstract void init();

    public abstract void preAct(float delta);

    public abstract void postAct(float delta);

    public abstract void preDraw();

    public abstract void postDraw();

    protected void registerEntity(VJXEntity e) {
        entities.put(e.getID(), e);
        e.addListener(game.getScreen().entityListener);
    }

    protected void unregisterEntity(VJXEntity e) {
        unregisterEntity(e.getID());
    }

    protected void unregisterEntity(long id) {
        entities.remove(id).removeListener(game.getScreen().entityListener);
    }

    @Override
    public void addActor(Actor actor) {
        if (actor instanceof VJXEntity) {
            registerEntity((VJXEntity) actor);
        }

        if (actor instanceof VJXGroup) {
            SnapshotArray<Actor> children = ((Group) actor).getChildren();
            for (Actor a : children) {
                addActor(a);
            }
        }
        if (actor.getParent() == null) super.addActor(actor);
    }

    void prepare() {
        init();
        prepared = true;
    }

    public Player getPlayer() {
        return player;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public VJXGame getGame() {
        return game;
    }

    public ArrayList<AssetDef<? extends Object>> getAssetList() {
        return resources;
    }

    public boolean isPrepared() {
        return prepared;
    }

    @Override
    public void dispose() {
        super.dispose();

        prepared = false;
    }

    @Override
    public String toString() {
        String s = this.getClass().getSimpleName() + " - " + getName();
        s += " - prepared: " + prepared;

        return s;
    }
}