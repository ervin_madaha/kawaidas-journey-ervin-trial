package de.venjinx.core.libgdx;

import com.badlogic.gdx.math.Vector2;

public class VJXTypes {

    public enum VJXStatus {

        // passive blocking
        // actions
        SPAWN("spawn", 0, true), HIT("hit", 1, true), DIE("die", 2, true),

        // passive non-blocking
        // actions
        TRIGGERED("triggered", 3, false),

        // active blocking

        // active non-blocking
        // activity
        IDLE("idle", 4, false), WALK("walk", 5, false),
        RUN("run", 6, false), FLY("fly", 7, false),
        SWIM("swim", 8, false), CLIMB("climb", 9, false),

        // actions
        JUMP("jump", 10, false), ATTACK("attack", 11, false),
        THROW("throw", 12, false);

        public final String name;
        public final int id;
        public final boolean blocking;

        private VJXStatus(String name, int id, boolean blocking) {
            this.name = name;
            this.id = id;
            this.blocking = blocking;
        }

        public static final VJXStatus get(int id) {
            for (VJXStatus s : VJXStatus.values())
                if (s.id == id) return s;

            return null;
        }

        public static final VJXStatus get(String name) {
            for (VJXStatus s : VJXStatus.values())
                if (s.name.equals(name)) return s;

            return null;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " " + id + ": " + name + " - "
                            + blocking;
        }
    };

    public enum VJXPhys {
        REMOVE("remove", Short.MIN_VALUE), NONE("none", (short) 0),
        PLAYER("player", (short) 1), CHARACTER("character", (short) 2),
        ITEM("item", (short) 4), TRIGGER("trigger",(short) 8),
        ENEMY("enemy", (short) 16), NO_PASS("no_pass", (short) 32),
        WEAPON("weapon", (short) 64), ALL("all", Short.MAX_VALUE);

        public final String name;
        public final short id;

        private VJXPhys(String name, final short id) {
            this.name = name;
            this.id = id;
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };

    public enum VJXDirection {
        Right("right", new Vector2(1f, 0f)), Up("up", new Vector2(0f, 1f)),
        Left("left", new Vector2(-1f, 0f)), Down("down", new Vector2(0f, -1f));

        public final String name;
        public final Vector2 direction;

        //        public final boolean flipX;
        //        public final boolean flipY;

        private VJXDirection(String name, Vector2 direction) {
            this.name = name;
            this.direction = direction;
        }

        @Override
        public String toString() {
            return name + " - " + getClass().getSimpleName();
        }
    };
}