package de.venjinx.core.libgdx.assets;

import com.badlogic.gdx.assets.AssetDescriptor;

public class AssetDef<T> extends AssetDescriptor<T> {
    public final String name;

    public AssetDef(String name, String fileName, Class<T> assetType) {
        super(fileName, assetType);
        this.name = name;
    }
}