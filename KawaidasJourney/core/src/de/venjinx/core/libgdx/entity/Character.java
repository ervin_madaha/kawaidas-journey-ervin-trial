package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.kawaida.kjgame.animation.Animation;
import de.kawaida.kjgame.animation.AnimationSet;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;

public abstract class Character extends VJXEntity {

    private FixtureDef headDef;
    private FixtureDef feetDef;
    private Fixture headFixture;
    private Fixture feetFixture;
    protected float moveSpeed = 1f;
    protected AnimationSet animations;

    public Character(BodyType type) {
        super(type);
        b2dCategory = VJXPhys.CHARACTER.id;
        b2dMask = (short) (VJXPhys.NO_PASS.id | VJXPhys.TRIGGER.id);

        headDef = new FixtureDef();
        headDef.restitution = 0f;
        headDef.friction = 0f;
        headDef.isSensor = true;

        feetDef = new FixtureDef();
        feetDef.restitution = 0f;
        feetDef.friction = 0f;
        feetDef.isSensor = true;
    }

    public void setAnimations(AnimationSet anims) {
        animations = anims;

        setSize(animations.getSize().x, animations.getSize().y);
        //        setSize(frame.getRegionWidth(), frame.getRegionHeight());

        setAnimation(getName() + "_" + status.name);
    }

    public void setAnimation(String animName) {
        Animation anim = animations.getCurrent();

        if (anim != null && animName.equals(anim.getName())) return;

        anim = animations.setAnimation(animName, "");
        anim.reset();

        frame = animations.getFrame();

        if (frame != null && getStage() != null)
            resizeBound(anim.getSize().x / 1.5f, anim.getSize().y / 1.5f,
                            anim.getOffset().x, anim.getOffset().y);
        updateDrawTransform();
    }

    @Override
    public void act(float deltaT) {
        //        if (status.blocking)
        if (statusChanged()) {
            animations.setAnimation(getName() + "_" + status.name, "");
            setUnchanged();
        }

        animations.update(deltaT);
        setFrame(animations.getFrame());

        super.act(deltaT);
    }

    @Override
    protected void createFixtures() {
        Animation anim = animations.getCurrent();
        Vector2 size = new Vector2(anim.getSize());

        size.scl(.75f);
        resizeBound(size.x, size.y,
                    anim.getOffset().x, anim.getOffset().y);

        PolygonShape feetShape, headShape;

        float halfW, halfH;
        Vector2 center = new Vector2();

        halfW = size.x / 2f;
        halfH = size.y / 2f;

        center.set(getX(), getY());
        center.add(halfW, halfH).sub(center);
        center.sub(0, halfH);
        center.scl(1f / 100f);

        feetShape = new PolygonShape();
        feetShape.setAsBox(halfW / 150f, .05f, center, 0);

        feetDef.shape = feetShape;
        feetDef.filter.categoryBits = VJXPhys.CHARACTER.id;
        feetDef.filter.maskBits = VJXPhys.NO_PASS.id;

        feetFixture = body.createFixture(feetDef);
        feetFixture.setUserData(new VJXData("feet", this));

        feetShape = (PolygonShape) feetFixture.getShape();

        center.add(0, size.y / 100f);
        headShape = new PolygonShape();
        headShape.setAsBox(halfW / 150f, halfH / 200f, center, 0);

        headDef.shape = headShape;
        headDef.filter.categoryBits = VJXPhys.TRIGGER.id;
        headDef.filter.maskBits = VJXPhys.CHARACTER.id;

        headFixture = body.createFixture(headDef);
        headFixture.setUserData(new VJXData(getName() + "_head", this));

        headShape = (PolygonShape) headFixture.getShape();

        //        PBELoader loader = new PBELoader(Gdx.files.internal(
        //                        "sprites/characters/kawaida/kawaida_hitboxes.json"));

        //        loader.attachFixture(body, "kawaida_dead#0.png", fixDef, 2,
        //                        (VJXData) boundFixture.getUserData());
    }

    public float getSpeed() {
        return moveSpeed;
    }

    public void setSpeed(float speed) {
        moveSpeed = speed;
    }

    public boolean isDead() {
        Animation anim = animations.getCurrent();
        //        return anim.getName().contains("die");
        return anim.getName().contains("die") && anim.isFinished();
    }

    public AnimationSet getAnimations() {
        return animations;
    }
}