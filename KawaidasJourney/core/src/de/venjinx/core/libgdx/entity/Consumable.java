package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Consumable extends Item {

    public Consumable(String name, BodyType type) {
        super(name, type);
    }

}
