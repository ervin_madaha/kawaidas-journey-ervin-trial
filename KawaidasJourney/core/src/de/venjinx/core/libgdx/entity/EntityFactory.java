package de.venjinx.core.libgdx.entity;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import de.kawaida.kjgame.animation.AnimationSet;
import de.kawaida.kjgame.entities.actors.Avatar;
import de.kawaida.kjgame.entities.actors.Ground;
import de.kawaida.kjgame.entities.enemies.Bird;
import de.kawaida.kjgame.entities.enemies.Trap;
import de.kawaida.kjgame.entities.enemies.Turtle;
import de.kawaida.kjgame.entities.items.Banana;
import de.kawaida.kjgame.entities.items.Cocos;
import de.kawaida.kjgame.entities.items.PalmTree;
import de.kawaida.kjgame.entities.items.Sun;
import de.kawaida.kjgame.entities.items.Totem;
import de.kawaida.kjgame.entities.neutral.Saidi;
import de.venjinx.core.libgdx.Player;
import de.venjinx.core.libgdx.VJXAssetManager;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.assets.AvatarIcon;

public class EntityFactory {

    private HashMap<Long, VJXEntity> entities = new HashMap<>();
    private VJXAssetManager am;
    private VJXGame game;

    public EntityFactory(VJXGame game) {
        this.game = game;
        am = game.assetMngr;
    }

    public VJXEntity createEntity(String name) {
        VJXEntity e;
        Player p;
        Avatar a;
        AvatarIcon aIcon;

        if (name == null || name.isEmpty()) return null;

        TextureAtlas ta;
        AnimationSet anims;

        switch (name) {
        case "palm":
            e = new PalmTree();
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("palm_idle").setLoop(true);
            anims.getAnimation("palm_triggered").setDelay(1f / 8f);
            anims.getAnimation("palm_grown").setLoop(true);

            ((Character) e).setAnimations(anims);
            break;
        case "colobus":
            e = new Saidi();
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("colobus_idle").setLoop(true);

            ta = am.getSpriteSheet("icons_avatars");
            aIcon = new AvatarIcon(am.drawableFromSheet("icons_avatars",
                            "icon_avatar_saidi"));

            e.setAvatarIcon(aIcon);

            ((Character) e).setAnimations(anims);
            break;
        case "kawaida":
            p = game.getPlayer();
            a = new Avatar(game);

            ta = game.assetMngr.getSpriteSheet("anims_" + name);
            anims = new AnimationSet(a);
            anims.load(ta);
            anims.getAnimation("kawaida_walk").setLoop(true);
            anims.getAnimation("kawaida_walk").setDelay(1 / 7f);
            anims.getAnimation("kawaida_run").setLoop(true);
            anims.getAnimation("kawaida_run").setDelay(1 / 7f);

            anims.getAnimation("kawaida_idle").setLoop(true);
            anims.getAnimation("kawaida_idle").setLoopBack(true);
            anims.getAnimation("kawaida_hit").setRepeat(3);
            anims.getAnimation("kawaida_hit").setDelay(1f / 5f);

            a.setAnimations(anims);

            a.addSoundFX("fx_kawaida_walk", game.assetMngr.getSound("fx_kawaida_walk"));
            a.addSoundFX("fx_kawaida_jump", game.assetMngr.getSound("fx_kawaida_jump"));
            a.addSoundFX("fx_kawaida_die", game.assetMngr.getSound("fx_kawaida_die"));
            a.addSoundFX("fx_kawaida_hit", game.assetMngr.getSound("fx_kawaida_hit"));
            a.addSoundFX("fx_kawaida_throw", game.assetMngr.getSound("fx_kawaida_throw"));
            a.addSoundFX("fx_kawaida_land_ground", game.assetMngr.getSound("fx_kawaida_land_ground"));

            p.setAvatar(a);

            aIcon = new AvatarIcon(am.drawableFromSheet("icons_avatars",
                            "icon_avatar_kawaida"));
            p.getAvatar().setAvatarIcon(aIcon);

            e = a;
            break;
        case "item_cocos_weapon":
            e = new Cocos("item_cocos_weapon");
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("item_cocos_weapon_idle").setLoop(true);
            ((Character) e).setAnimations(anims);

            e.setMask(VJXPhys.ENEMY.id);
            ((Item) e).setMax(10);
            //            e.setFrame(game.assetMngr.getRegion("collectables", "item_cocos"));

            e.addSoundFX("fx_item_cocos_hit",
                            game.assetMngr.getSound("fx_item_cocos_hit"));

            break;
        case "ground":
            e = new Ground(game.getScreen().getLevel().getLvlWidth(), 200);
            break;
        case "item_sun":
            e = new Sun(name);
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("item_sun_idle").setLoop(true);
            ((Character) e).setAnimations(anims);

            //            e.setFrame(am.getRegion("collectables", name));

            e.addSoundFX("fx_" + name + "_collected",
                            am.getSound("fx_" + name + "_collected"));
            break;
        case "item_banana":
            e = new Banana(name);
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("item_banana_idle").setLoop(true);
            ((Character) e).setAnimations(anims);

            //            e.setFrame(am.getRegion("collectables", name));

            e.addSoundFX("fx_" + name + "_collected",
                            am.getSound("fx_" + name + "_collected"));
            break;
        case "item_cocos":
            e = new Cocos(name);
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("item_cocos_idle").setLoop(true);
            ((Character) e).setAnimations(anims);

            //            e.setFrame(am.getRegion("collectables", name));

            e.addSoundFX("fx_" + name + "_collected",
                            am.getSound("fx_" + name + "_collected"));
            break;
        case "item_totem":
            e = new Totem(name);
            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("item_totem_idle").setLoop(true);
            ((Character) e).setAnimations(anims);

            //            e.setFrame(am.getRegion("collectables", name));

            e.addSoundFX("fx_" + name + "_collected",
                            am.getSound("fx_" + name + "_collected"));
            break;
        case "jz_turtle1":
            e = new Turtle();
            e.setName(name);

            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            ((Character) e).setAnimations(anims);
            anims.getAnimation("jz_turtle1_walk").setLoop(true);
            break;
        case "jz_bird1":
            e = new Bird();
            e.setName(name);

            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("jz_bird1_idle").setLoop(true);
            anims.getAnimation("jz_bird1_fly").setDelay(1f / 4.5f);
            anims.getAnimation("jz_bird1_fly").setLoop(true);
            ((Character) e).setAnimations(anims);

            e.addSoundFX("fx_jz_bird1_fly", am.getSound("fx_jz_bird1_fly"));
            break;
        case "jz_trap1":
            e = new Trap();
            e.setName(name);

            ta = am.getSpriteSheet("anims_" + name);
            anims = new AnimationSet((Character) e);
            anims.load(ta);
            anims.getAnimation("jz_trap1_idle").setLoop(true);
            anims.getAnimation("jz_trap1_triggered").setLoop(true);
            ((Character) e).setAnimations(anims);
            break;
        default:
            return null;
        }

        e.setName(name);

        entities.put(e.getID(), e);

        return e;
    }

    public void destroy(long id) {
        if (!entities.containsKey(id)) return;

        VJXEntity e = entities.remove(id);

        e.remove();
        e.despawn();
    }
}