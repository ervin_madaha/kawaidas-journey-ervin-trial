package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;

import de.venjinx.core.libgdx.VJXTypes.VJXPhys;


public abstract class Item extends Character {

    private int amount = 0;
    private int maxAmount = Integer.MAX_VALUE;

    public Item(String name, BodyType type) {
        super(type);
        b2dCategory = VJXPhys.ITEM.id;
        b2dMask = VJXPhys.PLAYER.id;

        setName(name);
    }

    //    public void newInstance(Vector2 position) {
    //        int instanceID = -1;
    //
    //        FixtureDef instanceFixtureDef = new FixtureDef();
    //
    //        Fixture instanceFixture;
    //
    //    }

    public void drawInstances() {

    }

    @Override
    public void collideWith(VJXEntity other) {
        other.collideWith(this);
    }

    @Override
    public void trigger(Fixture triggerFixture) {
    }

    @Override
    protected void createFixtures() {
        boundFixture.setSensor(true);
        resizeBound();
    }

    //    @Override
    //    public void userDraw(Batch batch, float parentAlpha) {
    //        batch.draw(frame, frame.getRegionWidth(), frame.getRegionHeight(),
    //                        drawTransform);
    //    }

    public void incr() {
        amount = amount + 1 <= maxAmount ? amount + 1 : amount;
    }

    public void decr() {
        amount = amount <= 0 ? 0 : amount - 1;
    }

    public void set(int amount) {
        if (amount < 0) this.amount = 0;
        else if (amount > maxAmount) this.amount = maxAmount;
        else this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setMax(int newMax) {
        maxAmount = newMax;

        if (newMax >= 0) amount = amount > maxAmount ? maxAmount : amount;
    }

    public int getMax() {
        return maxAmount;
    }

    public boolean isEmpty() {
        return amount == 0;
    }
}