package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Obstacle extends VJXEntity {

    public Obstacle(BodyType type) {
        super(type);
    }

}
