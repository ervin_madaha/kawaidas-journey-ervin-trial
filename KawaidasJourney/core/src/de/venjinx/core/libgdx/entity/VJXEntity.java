package de.venjinx.core.libgdx.entity;

import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import de.kawaida.kjgame.entities.actors.Enemy;
import de.kawaida.kjgame.interfaces.Acceptor;
import de.kawaida.kjgame.interfaces.Visitor;
import de.venjinx.core.libgdx.VJXData;
import de.venjinx.core.libgdx.VJXGame;
import de.venjinx.core.libgdx.VJXStage;
import de.venjinx.core.libgdx.VJXTypes.VJXDirection;
import de.venjinx.core.libgdx.VJXTypes.VJXPhys;
import de.venjinx.core.libgdx.VJXTypes.VJXStatus;
import de.venjinx.core.libgdx.assets.AvatarIcon;

public abstract class VJXEntity extends Actor implements Visitor, Acceptor {

    private long id = -1;

    protected boolean alive = false;
    protected boolean spawned = false; // body in world, spawn, despawn
    protected short b2dCategory = 0;
    protected short b2dMask = 0;

    protected VJXStatus status = VJXStatus.IDLE;
    protected VJXDirection faceDirection = VJXDirection.Right;
    protected int onGround = 0;

    protected HashSet<Long> ids;
    protected HashMap<String, Sound> sounds;
    protected HashMap<String, Long> soundIDs;
    protected AvatarIcon avatarIcon;
    protected AtlasRegion frame;

    protected FixtureDef boundDef;
    protected Fixture boundFixture;
    protected Shape boundShape;

    protected BodyType type;
    protected Body body;

    protected Affine2 drawTransform = new Affine2();

    public VJXEntity(BodyType type) {
        this.type = type;
        id = VJXGame.newID();
        setName("entity_" + id);

        boundDef = new FixtureDef();
        boundDef.restitution = 0f;
        boundDef.friction = 0f;

        ids = new HashSet<Long>();
        sounds = new HashMap<>();
        soundIDs = new HashMap<>();
    }

    //    public SpriteDrawable createIcon(VJXAssetManager am) {
    //        return am.createSpriteDrawable(getName() + "_icon");
    //    }

    public void setAvatarIcon(AvatarIcon icon) {
        avatarIcon = icon;
    }

    public Body spawnBody(World world) {
        destroyBody();
        despawn();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = type;

        float cx = getX() + getOriginX();
        float cy = getY() + getOriginY();

        body = world.createBody(bodyDef);
        body.setSleepingAllowed(false);
        body.setUserData(new VJXData(getName(), this));
        body.setTransform(cx / 100f, cy / 100f, 0);

        //        setStatus(VJXStatus.SPAWN, true);
        setStatus(VJXStatus.IDLE, true);

        createBound();
        createFixtures();

        //        spawnActor(world);

        spawned = true;
        alive = true;
        return body;
    }

    public void destroyBody() {
        if (body != null) {
            body.getWorld().destroyBody(body);
            body = null;
        }
    }

    public void despawn() {
        spawned = false;
    }

    @Override
    public void act(float deltaT) {
        Vector2 newPos = new Vector2(body.getPosition()).scl(100f);
        newPos.sub(getWidth() / 2, getHeight() / 2);

        super.setPosition(newPos.x, newPos.y);

        super.act(deltaT);

        userAct(deltaT);

        updateDrawTransform();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (frame != null)
            batch.draw(frame, frame.getRegionWidth(),
                            frame.getRegionHeight(), drawTransform);
        userDraw(batch, parentAlpha);
    }

    public abstract void collideWith(VJXEntity other);

    public abstract void trigger(Fixture triggerFixture);

    public abstract void userAct(float deltaT);

    public abstract void userDraw(Batch batch, float parentAlpha);

    protected abstract void createFixtures();

    public abstract void spawnActor(World world);

    @Override
    public void accVisitor(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void accCollision(Visitor visitor) {
        visitor.hit(this);
    }

    @Override
    public void accTrigger(Visitor visitor) {
        visitor.trigger(this);
    }

    public void addSoundFX(String name, Sound sound) {
        sounds.put(name, sound);
    }

    public long playSound(String name, boolean loop) {
        return playSound(name, loop, 1, 1, 0);
    }

    public long playSound(String name, boolean loop,
                    float volume, float pitch, float pan) {
        Sound s = sounds.get(name);
        boolean playing = soundIDs.containsKey(name);

        if (playing && loop) return soundIDs.get(name);

        if (playing && !loop) s.stop();

        long id;
        if (loop)
            id = s.loop(volume, pitch, pan);
        else id = s.play(volume, pitch, pan);

        soundIDs.put(name, id);
        return id;
    }

    public void stopSound(String name) {
        if (!soundIDs.containsKey(name)) return;

        sounds.get(name).stop(soundIDs.remove(name));
    }

    public void stopAllSounds() {
        for (String s : sounds.keySet())
            stopSound(s);
    }

    public void setFrame(AtlasRegion f) {
        frame = f;
        if (frame != null) {
            setSize(frame.originalWidth, frame.originalHeight);
        }
    }

    protected void resizeBound() {
        resizeBound(frame);
    }

    protected void resizeBound(AtlasRegion frame) {
        if (frame != null)
            resizeBound(frame.getRegionWidth(),
                        frame.getRegionHeight(),
                        frame.offsetX, frame.offsetY);
    }

    protected void resizeBound(float w, float h, float offX, float offY) {
        if (boundShape != null) {
            Vector2 center = new Vector2(0f, 0f);

            center.set(getX(), getY());
            center.add(offX, offY).add(getWidth() / 2f, getHeight() / 2f);
            center.sub(getCenter());
            center.scl(1f / 100f);

            if (boundShape instanceof PolygonShape) {
                ((PolygonShape) boundShape).setAsBox(w / 200f, h / 200f, center, 0);
                return;
            }

            if (boundShape instanceof ChainShape) {
                //                ((ChainShape) boundShape).createChain(new Vector2[] {
                //                                new Vector2(0, offY / 100f),
                //                                new Vector2(w / 100f, offY / 100f) });
                return;
            }
        }
    }

    //    public Body createBody(World world) {
    //        //        System.out.println("create body " + this);
    //        //        BodyDef bodyDef = new BodyDef();
    //        //        bodyDef.type = type;
    //        //
    //        //        float cx = getX() + getOriginX();
    //        //        float cy = getY() + getOriginY();
    //        //        body = world.createBody(bodyDef);
    //        //        body.setSleepingAllowed(false);
    //        //        body.setUserData(new VJXData(getName(), this));
    //        //        body.setTransform(cx / 100f, cy / 100f, 0);
    //
    //        //        MassData data = new MassData();
    //        //        data.mass = 0;
    //        //        data.center.set(body.getWorldCenter());
    //        //        body.setMassData(data);
    //
    //        createBound();
    //
    //        createFixtures();
    //
    //        alive = true;
    //        return body;
    //    }

    public void setStatus(VJXStatus status) {
        setStatus(status, false);
    }

    private boolean statusChanged = false;

    public boolean statusChanged() {
        return statusChanged;
    }

    public void setUnchanged() {
        statusChanged = false;
    }

    public void setStatus(VJXStatus status, boolean force) {
        if (this.status.blocking && !force) return;
        this.status = status;
        statusChanged = true;

        if (status == VJXStatus.DIE) {
            alive = false;
            body.setLinearVelocity(0, 0);

            for (Fixture f : body.getFixtureList())
                if (this instanceof Enemy) {
                    b2dCategory = VJXPhys.CHARACTER.id;
                    b2dMask = VJXPhys.NO_PASS.id;

                    Filter filter = new Filter();
                    filter.categoryBits = b2dCategory;
                    filter.maskBits = b2dMask;
                    f.setFilterData(filter);
                }
            stopAllSounds();
        }
    }

    public void setDirection(VJXDirection direction) {
        faceDirection = direction;
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        updateDrawTransform();

        if (body != null) {
            body.setTransform(drawTransform.getTranslation(new Vector2())
                            .add(getWidth() / 2f, getHeight() / 2f)
                            .scl(1 / 100f), 0);

        }
    };

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        setOrigin(width / 2f, height / 2f);
    }

    public void resetOnGround() {
        onGround = 0;
    }

    public void incrOnGround() {
        onGround++;
    }

    public void decrOnGround() {
        onGround = onGround > 0 ? onGround - 1 : onGround;
    }

    public boolean isOnGround() {
        return onGround > 0;
    }

    public long getID() {
        return id;
    }

    public VJXStatus getStatus() {
        return status;
    }

    public void setCategory(short category) {
        b2dCategory = category;
    }

    public short getCategory() {
        return b2dCategory;
    }

    public void setMask(short mask) {
        b2dMask = mask;
    }

    public short getCollisionMask() {
        return b2dMask;
    }

    public AvatarIcon getAvatarIcon() {
        return avatarIcon;
    }

    public Body getBody() {
        return body;
    }

    public float getDrawX() {
        if (frame == null) return 0f;
        return getX() + frame.offsetX;
    }

    public float getDrawY() {
        if (frame == null) return 0f;
        return getX() + frame.offsetX;
    }

    public Vector2 getDrawPosition() {
        if (frame == null) return new Vector2(getX(), getY());

        return new Vector2(getX() + frame.offsetX,
                           getY() + frame.offsetY);
    }

    public Vector2 getCenter() {
        return new Vector2(getX() + getOriginX(), getY() + getOriginY());
    }

    @Override
    public VJXStage getStage() {
        return (VJXStage) super.getStage();
    }

    public VJXDirection getFaceDirection() {
        return faceDirection;
    }

    //    public void setAlive(boolean alive) {
    //        this.alive = alive;
    //    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isSpawned() {
        return spawned;
    }

    protected void updateDrawTransform() {
        if (frame == null) return;
        // update the draw transform, draw pos is bottom left corner of object
        drawTransform.idt();
        drawTransform.translate(getX(), getY());

        // mirror the image if object is facing left
        if (faceDirection == VJXDirection.Left) {
            drawTransform.translate(frame.getRegionWidth(), 0f);
            drawTransform.scale(-1, 1);
        }
    }

    private void createBound() {
        float halfW, halfH;
        Vector2 center = new Vector2();

        halfW = getWidth() / 2f;
        halfH = getHeight() / 2f;

        center.set(getX(), getY());
        center.add(halfW, halfH).sub(center);
        center.scl(1f / 100f);

        boundShape = new PolygonShape();
        ((PolygonShape) boundShape).setAsBox(halfW / 100f, halfH / 100f, center, 0);

        boundDef.filter.categoryBits = b2dCategory;
        boundDef.filter.maskBits = b2dMask;
        boundDef.shape = boundShape;

        boundFixture = body.createFixture(boundDef);
        boundFixture.setUserData(new VJXData("bound", this));

        boundShape = boundFixture.getShape();
    }

    public String getInfo() {
        String s = toString();

        s += "\n  BodyType   : " + type;
        s += "\n  Status     : " + status;
        s += "\n  Face       : " + faceDirection;
        s += "\n  Category   : " + b2dCategory;
        s += "\n  Size       : [" + getWidth() + ":" + getHeight() + "]";
        s += "\n  Position   : [" + getX() + ":" + getY() + "]";
        s += "\n  DrawPos    : " + getDrawPosition();
        s += "\n  Center     : " + getCenter();
        s += "\n  Origin     : [" + getOriginX() + ":" + getOriginY() + "]";
        s += "\n  UserData   : " + getUserObject();
        s += "\n  Body       : " + body;
        if (body != null) {
            s += "\n  BodyCenter : " + body.getLocalCenter();
            s += "\n  BodyPos    : " + body.getPosition();
            s += "\n  BodyTrans  : " + body.getTransform().getPosition();
            s += "\n  BodyWCenter: " + body.getWorldCenter();
        }

        s += "\n";

        return s;
    }

    @Override
    public String toString() {
        return getName();
        //        return getInfo();
    }
}