package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Valuable extends Item {

    public Valuable(String name) {
        super(name, BodyType.StaticBody);

    }

}
