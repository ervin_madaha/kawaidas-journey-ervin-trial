package de.venjinx.core.libgdx.entity;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.venjinx.core.libgdx.VJXTypes.VJXPhys;

public abstract class Weapon extends Item {

    public Weapon(String name, BodyType type) {
        super(name, type);
        b2dCategory |= VJXPhys.WEAPON.id;
        //        b2dMask = VJXPhys.ENEMY.id;
    }

}