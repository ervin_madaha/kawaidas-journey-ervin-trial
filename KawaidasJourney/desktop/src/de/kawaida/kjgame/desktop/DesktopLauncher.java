package de.kawaida.kjgame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.venjinx.core.libgdx.VJXGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration lwjglCfg = new LwjglApplicationConfiguration();

        lwjglCfg.title = "Kawaida - Game Workshop Tansania";
        lwjglCfg.width = 960;
        lwjglCfg.height = 540;
        lwjglCfg.foregroundFPS = 60;

        //        arg = new String[1];
        //        arg[0] = "D:/Development/Projekte/Kawaida/Dev/kawaidas-journey/"
        //                        + "KawaidasJourney/android/assets/levels/"
        //        //                        + "devel/devel#01.tmx";
        //                        + "jozani/jozani_prototype#03.tmx";

        // Start Game
        new LwjglApplication(new VJXGame(arg), lwjglCfg);
        //        new LwjglApplication(new ParaTest(), lwjglCfg);
    }
}