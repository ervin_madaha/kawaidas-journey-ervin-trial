package de.kawaida.workshops.tza;

import java.util.HashSet;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import de.kawaida.workshops.tza.characters.Player;
import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.controllers.CollisionControl;
import de.kawaida.workshops.tza.controllers.TextureManager;
import de.kawaida.workshops.tza.debug.DebugView;
import de.kawaida.workshops.tza.controllers.InputControl;
import de.kawaida.workshops.tza.screens.IntroScreen;
import de.kawaida.workshops.tza.screens.ScreenManager;
import de.kawaida.workshops.tza.config.InputConfig;

public class WorkshopGameTZA implements ApplicationListener {
	
	private float timePassed = 0;
	
	private float step = 1 / 60f;

	public OrthographicCamera cam;
	public OrthographicCamera b2dCam;
	public CollisionControl ch;
	private DebugView debug;
	
	public static SpriteBatch batch;
	public static BitmapFont font;
	public static TextureManager assetManager;

	public Player player;
	public World world;
	public static HashSet<Body> bodiesToRemove;
	
	IntroScreen iScreen;
	
	@Override
	public void create () {
		cam = new OrthographicCamera();
		cam.setToOrtho(false, GameCfg.RES.x, GameCfg.RES.y);
		
		b2dCam = new OrthographicCamera();
		b2dCam.setToOrtho(false, GameCfg.RES.x / 100f, GameCfg.RES.y / 100f);
		
		GameCfg.APP_RES.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		GameCfg.ASPECT = GameCfg.RES.x / GameCfg.RES.y;
		
		GameCfg.SCALE.x = GameCfg.APP_RES.x / GameCfg.RES.x;
		GameCfg.SCALE.y = GameCfg.APP_RES.y / GameCfg.RES.y;
		
		System.out.println(GameCfg.printCfg());
		
		Gdx.input.setInputProcessor(new InputControl());
		
		assetManager = new TextureManager();
		font = new BitmapFont();
		
		batch = new SpriteBatch();
		batch.setProjectionMatrix(cam.combined);
		
		world = new World(new Vector2(0, -3.81f), true);
		world.setContactListener(ch = new CollisionControl(this));
		
		bodiesToRemove = new HashSet<Body>();

		player = new Player(world);
		ScreenManager.setScreen(iScreen = new IntroScreen(this));
		
		debug = new DebugView(this);
		debug.init();
		
//		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void render () {
//		System.out.println("------------------begin main loop------------------");
		
		timePassed += Gdx.graphics.getDeltaTime();
		cam.update();
		b2dCam.update();
		
		Gdx.gl20.glClearColor(0, 0, 0, 1);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		if (timePassed >= step) {
//			System.out.println("----------------------update-----------------------");

			ScreenManager.getScreen().update(step);
			InputConfig.update();
			debug.update(step);

			world.step(step, 6, 2);
			timePassed -= step;
		}

//		System.out.println("----------------------render-----------------------");
		ScreenManager.getScreen().render();
		
		batch.begin();

		font.setScale(1.5f);
		font.setColor(Color.BLACK);
		font.draw(batch, "Life : " + player.life, 10, 470);
		font.draw(batch, "Score: " + player.score, 10, 500);
		
		batch.end();
		
		debug.render();
		
//		if (Gdx.input.isKeyPressed(Keys.BACK)) {
//			dispose();
//			Gdx.app.exit();
//		}
		
//		System.out.println("-------------------end main loop-------------------\n");
	}

	@Override
	public void dispose() {
		if (ScreenManager.getScreen() != null)
			ScreenManager.getScreen().dispose();
		font.dispose();
		TextureManager.dispose();
		batch.dispose();
		debug.dbgRenderer.dispose();
	}

	@Override
	public void pause() {
		if (ScreenManager.getScreen() != null)
			ScreenManager.getScreen().pause();
	}

	@Override
	public void resume() {
		if (ScreenManager.getScreen() != null)
			ScreenManager.getScreen().resume();
	}

	@Override
	public void resize(int width, int height) {
		if (ScreenManager.getScreen() != null)
			ScreenManager.getScreen().resize(width, height);
		
		GameCfg.RES.x = width;
		GameCfg.RES.y = height;
		
		cam = new OrthographicCamera();
		cam.setToOrtho(false, GameCfg.RES.x, GameCfg.RES.y);
		
		b2dCam = new OrthographicCamera();
		b2dCam.setToOrtho(false, GameCfg.RES.x / 100f, GameCfg.RES.y / 100f);
	}
}