package de.kawaida.workshops.tza.animation;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class AnimationSet {
	
	private HashMap<String, Animation> animations = new HashMap<String, Animation>();
	private Animation currentAnimation;
	
	private Vector2 maxSize = new Vector2();

	public AnimationSet() {
		
	}

	public AnimationSet(TextureAtlas textureAtlas) {
		this(textureAtlas, 1 / 5f);
	}
	
	public AnimationSet(TextureAtlas textureAtlas, float delay) {
		setFrames(textureAtlas, delay);
	}
	
	public void setFrames(TextureAtlas textureAtlas, float delay) {
		Array<AtlasRegion> a = textureAtlas.getRegions();
		if (a.size == 0) return;
		
		String[] frameData;
		AtlasRegion frame;
		String frameName;
		int frameID, frameDirection;
		Animation anim;
		float w, h;
		AtlasRegion[] frames = new AtlasRegion[a.size];

		int count = 0, frameNr = 0;
		
		for (int i = 0; i < a.size; i++) {
			frame = a.get(i);
			frameData = frame.name.split("#");
			frameName = frameData[0];
			frameID = Integer.parseInt(frameData[1]);
			frames[i] = frame;
			
			w = frame.getRegionWidth();
			h = frame.getRegionHeight();
			
			maxSize.x = w > maxSize.x ? w : maxSize.x;
			maxSize.y = h > maxSize.y ? h : maxSize.y;

			anim = new Animation(frameName);
			anim.setFrames(frames, delay);
			anim.setStartFrame(frameNr);

			count++;
			frameNr++;
			while (i + count < a.size) {
				frame = a.get(i + count);
				frameName = frame.name.split("#")[0];
				frames[i + count] = frame;
				
				w = frame.getRegionWidth();
				h = frame.getRegionHeight();
				
				maxSize.x = w > maxSize.x ? w : maxSize.x;
				maxSize.y = h > maxSize.y ? h : maxSize.y;
				
				if (anim.getName().equals(frameName)) {
//					System.out.println(frameName);
					count++;
					frameNr++;
				} else {
					anim.setEndFrame(anim.getStartFrame() + count);
					animations.put(anim.getName(), anim);
//					System.out.println(anim);
					
					i--;
					break;
				}
			}
			anim.setEndFrame(anim.getStartFrame() + count);
			animations.put(anim.getName(), anim);
//			System.out.println("-------------------");
			i += count;
			count = 0;
		}
	}
	
	public void addAnimation(Animation animation, String animName) {
		animations.put(animName, animation);
	}
	
	public void setAnimation(String animName) {
		currentAnimation = animations.get(animName);
	}
	
	public HashMap<String, Animation> getAnimations() {
		return animations;
	}
	
	public Animation getCurrentAnimation() {
		return currentAnimation;
	}
	
	public int getAnimCount() {
		return animations.size();
	}
}