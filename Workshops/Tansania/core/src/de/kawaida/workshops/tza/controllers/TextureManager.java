package de.kawaida.workshops.tza.controllers;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class TextureManager {

	private static HashMap<String, TextureAtlas> textures = new HashMap<String, TextureAtlas>();
	
	public static TextureAtlas loadSpriteSheet(String path, String key) {
		if (textures.get(key) != null) return textures.get(key);
		TextureAtlas ta = new TextureAtlas(path);
		textures.put(key, ta);
		return ta;
	}
	
	public static TextureAtlas getTextureAtlas(String key) {
		return textures.get(key);
	}
	
	public static void disposeTexture(String key) {
		TextureAtlas tex = textures.remove(key);
		if (tex != null)
			tex.dispose();
	}
	
	public static void dispose() {
		for (TextureAtlas ta : textures.values())
			ta.dispose();
	}
}