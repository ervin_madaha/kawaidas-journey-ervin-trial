package de.kawaida.workshops.tza.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.kawaida.workshops.tza.WorkshopGameTZA;
import de.kawaida.workshops.tza.config.GameCfg;
import de.kawaida.workshops.tza.screens.ScreenManager;

public class DebugView {
	
	public Box2DDebugRenderer dbgRenderer;
	private	ShapeRenderer shapeRenderer;
	private WorkshopGameTZA game; 
	private World world;
	
	public DebugView(WorkshopGameTZA game) {
		this.game = game;
		this.world = game.world;
		
		dbgRenderer = new Box2DDebugRenderer();
		dbgRenderer.setDrawBodies(false);
		shapeRenderer = new ShapeRenderer();
	}
	
	public void init() {
		
	}
	
	public void update(float t) {
		
	}
	
	public void render() {
//		drawGrid();
		
		dbgRenderer.render(world, game.b2dCam.combined);
		
//		drawBodyCenters();
		
		WorkshopGameTZA.batch.begin();
		
		WorkshopGameTZA.font.setScale(1);
		WorkshopGameTZA.font.setColor(Color.WHITE);
		WorkshopGameTZA.font.draw(WorkshopGameTZA.batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 0, 20);
		
		WorkshopGameTZA.batch.end();
		
		ScreenManager.getScreen().debugRender();
	}
	
	private void drawGrid() {
		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.begin(ShapeType.Line);
		for (int x = 0; x < GameCfg.RES.x; x += 100) {
			shapeRenderer.line(x, 0, x, GameCfg.RES.y);
		}
		
		for (int y = 0; y < GameCfg.RES.y; y += 100) {
			shapeRenderer.line(0, y, GameCfg.RES.x, y);
		}
		shapeRenderer.end();
	}
	
	private void drawBodyCenters() {
		Array<Body> bodies = new Array<Body>();
		game.world.getBodies(bodies);

		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.begin(ShapeType.Filled);
		for (Body body : bodies) {
			shapeRenderer.circle(body.getPosition().x * 100, body.getPosition().y * 100, 2.5f);
		}
		shapeRenderer.end();
	}
}