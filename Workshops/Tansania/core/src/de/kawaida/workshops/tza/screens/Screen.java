package de.kawaida.workshops.tza.screens;

public abstract class Screen {

	public abstract void init();
	
	public abstract void update(float t);
	
	public abstract void render();
	
	public abstract void debugRender();
	
	public abstract void handleInput();
	
	public abstract void resize(int width, int height);
	
	public abstract void dispose();
	
	public abstract void pause();
	
	public abstract void resume();
}
